export class AppConstants {
    public static ROLE_ADMIN = 'ADMIN';
    public static ROLE_CLIENT = 'CLIENT';

    public static SOCKET_USER = 'socket_user';
    public static SOCKET_EVENT = 'socket_event'; 

    public static SOCKET_ROOM_ADMIN = 'socket_room_admin';

    public static TYPE_USER = 'User';
    public static TYPE_BOOKING = 'Booking';
    public static TYPE_CLIENT_VISITOR = 'Client_Visitor';
    public static TYPE_EVENT = 'Event';
    public static TYPE_FEEDBACK = 'Feedback';
    // public static TYPE_ACHIEVER = 'Achiever';
}
