export interface ResponseFormat {
    success: boolean;
    message: string;
    data?: Object;
    error?: any;
    token?: string;
}

export interface SocketEventFormat {
    type: string;
    message: string;
    url: string;
}

export interface FCMEventFormat {
    title: string,
    body: string,
    data: { [key: string]: string; },
    image?: string,
}