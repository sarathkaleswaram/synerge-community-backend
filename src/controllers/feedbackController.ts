import { Request, Response } from 'express';
import { logger } from '../lib/logger';
import { Feedback } from '../models/feedback';
import { FCMEventFormat, ResponseFormat, SocketEventFormat } from '../domains/response-format';
import { AppConstants } from '../domains/app-constants';
import SynergeSocket from '../lib/socket';
import { pushFcmNotification } from '../lib/firebase-notification';

export class FeedbackController {

    public responseFormat: ResponseFormat;

    public sendResponse(result: ResponseFormat, res: Response) {
        res.json(result);
    }

    public socketResponse(message: string, url: string, sendTo: string, id: string) {
        let socketResponse: SocketEventFormat = {
            type: AppConstants.TYPE_FEEDBACK,
            message: message,
            url: url
        };
        SynergeSocket.sendSocketResponse(socketResponse, sendTo, id);
    }

    public pushFCM(title: string, body: string, url: string, sendTo: string, id: string, image?: string) {
        let fcm: FCMEventFormat = {
            title,
            body,
            data: {
                path: url
            },
            image
        };
        pushFcmNotification(sendTo, id, fcm);
    }

    public addNewFeedback(req: Request, res: Response) {
        logger.debug("Add New Feedback", req.body);
        let newFeedback = new Feedback(req.body);

        newFeedback.save((err, feedback) => {
            if (err) {
                this.responseFormat = { success: false, message: err.message, error: err };
            } else {
                this.responseFormat = { success: true, message: 'Feedback added Successfully!', data: feedback };
                // this.socketResponse('New Feedback/Suggestion created', 'feedback?feedbackId=' + feedback._id, AppConstants.SOCKET_ROOM_ADMIN, undefined);
                this.socketResponse('New Feedback/Suggestion created', 'feedback?feedbackId=' + feedback._id, AppConstants.SOCKET_ROOM_ADMIN + feedback.belongs_to, undefined);
                this.pushFCM(
                    'Feedback/Suggestion created',
                    `User: ${feedback.by_user_name}, Feedback: ${feedback.feedback}.`,
                    `/feedback?id=${feedback._id}`,
                    AppConstants.ROLE_ADMIN + feedback.belongs_to,
                    undefined,
                    undefined
                );
            }
            return this.sendResponse(this.responseFormat, res);
        });
    }

    public async getAllFeedback(req: Request, res: Response) {
        try {
            logger.debug("Get all Feedback");
            let mongoQuery = {};
            if (res.locals.jwtPayload.role === AppConstants.ROLE_ADMIN) {
                if (res.locals.jwtPayload.belongs_to)
                    mongoQuery['belongs_to'] = res.locals.jwtPayload.belongs_to;
            } else {
                mongoQuery['by_user_id'] = res.locals.jwtPayload._id;
            }
            let data = await Feedback.find(mongoQuery).sort({ _id: -1 }).exec();
            return this.sendResponse({ success: true, message: 'Request Success', data }, res);
        } catch (error) {
            logger.error('Get all Feedback failed: ' + error);
            return this.sendResponse({ success: false, message: error.message || JSON.stringify(error) }, res);
        }
        // if (res.locals.jwtPayload.role == AppConstants.ROLE_ADMIN) {
        //     logger.debug("Get all Feedback");
        //     let mongoQuery = {};
        //     if (res.locals.jwtPayload.belongs_to)
        //         mongoQuery['belongs_to'] = res.locals.jwtPayload.belongs_to;
        //     Feedback.find(mongoQuery, (err, feedback) => {
        //         if (err) {
        //             this.responseFormat = { success: false, message: err.message, error: err };
        //         } else {
        //             this.responseFormat = { success: true, message: 'Request Success', data: feedback };
        //         }
        //         return this.sendResponse(this.responseFormat, res);
        //     });
        // } else {
        //     logger.debug("Get all user Feedback");
        //     Feedback.find({ by_user_id: res.locals.jwtPayload._id }, (err, feedback) => {
        //         if (err) {
        //             this.responseFormat = { success: false, message: err.message, error: err };
        //         } else {
        //             this.responseFormat = { success: true, message: 'Request Success', data: feedback };
        //         }
        //         return this.sendResponse(this.responseFormat, res);
        //     });
        // }
    }

    public getFeedbackWithID(req: Request, res: Response) {
        logger.debug("Get Feedback With Id: " + req.params.feedbackId);
        Feedback.findById(req.params.feedbackId, (err, feedback) => {
            if (err) {
                this.responseFormat = { success: false, message: err.message, error: err };
            } else {
                this.responseFormat = { success: true, message: 'Request Success', data: feedback };
            }
            return this.sendResponse(this.responseFormat, res);
        });
    }

    public updateFeedback(req: Request, res: Response) {
        logger.debug("Update Feedback Id: " + req.params.feedbackId + " Body", req.body);
        Feedback.findOneAndUpdate({ _id: req.params.feedbackId }, req.body, { new: true }, (err, feedback) => {
            if (err) {
                this.responseFormat = { success: false, message: err.message, error: err };
            } else {
                this.responseFormat = { success: true, data: feedback, message: 'Feedback updated Successfully!' };
                // this.socketResponse('Feedback/Suggestion updated', 'feedback?feedbackId=' + feedback._id, AppConstants.SOCKET_ROOM_ADMIN, undefined);
                this.socketResponse('Feedback/Suggestion updated', 'feedback?feedbackId=' + feedback._id, AppConstants.SOCKET_ROOM_ADMIN + feedback.belongs_to, undefined);
                this.socketResponse('Feedback/Suggestion updated', 'feedback?feedbackId=' + feedback._id, undefined, feedback.by_user_id);
                let title = 'Feedback/Suggestion updated';
                if (req.body.status) {
                    title = `Feedback/Suggestion ${req.body.status}`;
                }
                this.pushFCM(
                    title,
                    `User: ${feedback.by_user_name}, Feedback: ${feedback.feedback}, Status: ${feedback.status}`,
                    `/feedback?id=${feedback._id}`,
                    AppConstants.ROLE_ADMIN + feedback.belongs_to,
                    feedback.by_user_id,
                    undefined
                );
            }
            return this.sendResponse(this.responseFormat, res);
        });
    }

    public deleteFeedback(req: Request, res: Response) {
        logger.debug("Delete Feedback With Id: " + req.params.feedbackId);
        Feedback.findByIdAndDelete(req.params.feedbackId, (err, feedback) => {
            if (err) {
                this.responseFormat = { success: false, message: err.message, error: err };
            } else {
                this.responseFormat = { success: true, message: 'Feedback deleted Successfully!' };
                // this.socketResponse('Feedback/Suggestion deleted', 'feedback', AppConstants.SOCKET_ROOM_ADMIN, undefined);
                this.socketResponse('Feedback/Suggestion deleted', 'feedback', AppConstants.SOCKET_ROOM_ADMIN + feedback.belongs_to, undefined);
                this.socketResponse('Feedback/Suggestion deleted', 'feedback', undefined, feedback.by_user_id);
                this.pushFCM(
                    'Feedback/Suggestion deleted',
                    `User: ${feedback.by_user_name}, Feedback: ${feedback.feedback}, Status: ${feedback.status}`,
                    `/feedback?id=${feedback._id}`,
                    AppConstants.ROLE_ADMIN + feedback.belongs_to,
                    feedback.by_user_id,
                    undefined
                );
            }
            return this.sendResponse(this.responseFormat, res);
        });
    }
}