import { Request, Response } from 'express';
import { logger } from '../lib/logger';
import { Event } from '../models/events';
import { FCMEventFormat, ResponseFormat, SocketEventFormat } from '../domains/response-format';
import { AppConstants } from '../domains/app-constants';
import SynergeSocket from '../lib/socket';
import { pushFcmNotification } from '../lib/firebase-notification';

export class EventController {

    public responseFormat: ResponseFormat;

    public sendResponse(result: ResponseFormat, res: Response) {
        res.json(result);
    }

    public socketResponse(message: string, url: string, sendTo: string, id: string) {
        let socketResponse: SocketEventFormat = {
            type: AppConstants.TYPE_EVENT,
            message: message,
            url: url
        };
        SynergeSocket.sendSocketResponse(socketResponse, sendTo, id);
    }

    public pushFCM(title: string, body: string, url: string, sendTo: string, id: string, image?: string) {
        let fcm: FCMEventFormat = {
            title,
            body,
            data: {
                path: url
            },
            image
        };
        pushFcmNotification(sendTo, id, fcm);
    }

    public addNewEvent(req: Request, res: Response) {
        logger.debug("Add New Event", req.body);
        let newEvent = new Event(req.body);

        newEvent.save((err, event) => {
            if (err) {
                this.responseFormat = { success: false, message: err.message, error: err };
            } else {
                this.responseFormat = { success: true, message: 'Event added Successfully!', data: event };
                this.socketResponse('New Event created', 'events?eventId=' + event._id, AppConstants.SOCKET_ROOM_ADMIN, undefined);
                this.socketResponse('New Event created', 'events?eventId=' + event._id, event.belongs_to, undefined);
                this.pushFCM(
                    'New Event created',
                    `Title: ${event.title}. Description: ${event.description}. Type: ${event.type}`,
                    `/events?id=${event._id}`,
                    event.belongs_to,
                    undefined,
                    event.pics[0]
                );
            }
            return this.sendResponse(this.responseFormat, res);
        });
    }

    public async getAllEvents(req: Request, res: Response) {
        try {
            logger.debug("Get all Event");
            let mongoQuery = {};
            if (res.locals.jwtPayload.role !== AppConstants.ROLE_ADMIN)
                mongoQuery['belongs_to'] = res.locals.jwtPayload.belongs_to;
            let data = await Event.find(mongoQuery).sort({ _id: -1 }).exec();
            return this.sendResponse({ success: true, message: 'Request Success', data }, res);
        } catch (error) {
            logger.error('Get all Event failed: ' + error);
            return this.sendResponse({ success: false, message: error.message || JSON.stringify(error) }, res);
        }
        // if (res.locals.jwtPayload.role == AppConstants.ROLE_ADMIN) {
        //     logger.debug("Get all Event");
        //     Event.find((err, events) => {
        //         if (err) {
        //             this.responseFormat = { success: false, message: err.message, error: err };
        //         } else {
        //             this.responseFormat = { success: true, message: 'Request Success', data: events };
        //         }
        //         return this.sendResponse(this.responseFormat, res);
        //     });
        // } else {
        //     logger.debug("Get all BelongsTo Event");
        //     Event.find({ belongs_to: res.locals.jwtPayload.belongs_to }, (err, events) => {
        //         if (err) {
        //             this.responseFormat = { success: false, message: err.message, error: err };
        //         } else {
        //             this.responseFormat = { success: true, message: 'Request Success', data: events };
        //         }
        //         return this.sendResponse(this.responseFormat, res);
        //     });
        // }
    }

    public getEventWithID(req: Request, res: Response) {
        logger.debug("Get Event With Id: " + req.params.eventId);
        Event.findById(req.params.eventId, (err, event) => {
            if (err) {
                this.responseFormat = { success: false, message: err.message, error: err };
            } else {
                this.responseFormat = { success: true, message: 'Request Success', data: event };
            }
            return this.sendResponse(this.responseFormat, res);
        });
    }

    public updateEvent(req: Request, res: Response) {
        logger.debug("Update Event Id: " + req.params.eventId + " Body", req.body);
        Event.findOneAndUpdate({ _id: req.params.eventId }, req.body, { new: true }, (err, event) => {
            if (err) {
                this.responseFormat = { success: false, message: err.message, error: err };
            } else {
                this.responseFormat = { success: true, data: event, message: 'Event updated Successfully!' };
                this.socketResponse('Event updated', 'events?eventId=' + event._id, AppConstants.SOCKET_ROOM_ADMIN, undefined);
                this.socketResponse('Event updated', 'events?eventId=' + event._id, event.belongs_to, undefined);
                // this.pushFCM(
                //     'Event updated',
                //     `Title: ${event.title}. Description: ${event.description}.`,
                //     `/events?id=${event._id}`,
                //     event.belongs_to,
                //     undefined,
                //     event.pics[0]
                // );
            }
            return this.sendResponse(this.responseFormat, res);
        });
    }

    public deleteEvent(req: Request, res: Response) {
        logger.debug("Delete Event With Id: " + req.params.eventId);
        Event.findByIdAndDelete(req.params.eventId, (err, event) => {
            if (err) {
                this.responseFormat = { success: false, message: err.message, error: err };
            } else {
                this.responseFormat = { success: true, message: 'Event deleted Successfully!' };
                this.socketResponse('Event deleted', 'events', AppConstants.SOCKET_ROOM_ADMIN, undefined);
                this.socketResponse('Event deleted', 'events', event.belongs_to, undefined);
                // this.pushFCM(
                //     'New Event deleted',
                //     `Title: ${event.title}. Description: ${event.description}.`,
                //     `/events?id=${event._id}`,
                //     event.belongs_to,
                //     undefined,
                //     event.pics[0]
                // );
            }
            return this.sendResponse(this.responseFormat, res);
        });
    }
}