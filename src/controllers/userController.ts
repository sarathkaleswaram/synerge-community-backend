import { Request, Response } from 'express';
import * as jwt from 'jsonwebtoken';
import { config } from '../app';
import { logger } from '../lib/logger';
import { User, IUser } from '../models/user';
import { RoleBasedRoutes } from '../models/routes';
import { FCMEventFormat, ResponseFormat, SocketEventFormat } from '../domains/response-format';
import { AppConstants } from '../domains/app-constants';
import SynergeSocket from '../lib/socket';
import { pushFcmNotification } from '../lib/firebase-notification';

export class UserController {

    public responseFormat: ResponseFormat;

    public sendResponse(result: ResponseFormat, res: Response) {
        res.json(result);
    }

    public socketResponse(message: string, url: string, sendTo: string, id: string) {
        let socketResponse: SocketEventFormat = {
            type: AppConstants.TYPE_USER,
            message: message,
            url: url
        };
        SynergeSocket.sendSocketResponse(socketResponse, sendTo, id);
    }

    public pushFCM(title: string, body: string, url: string, sendTo: string, id: string, image?: string) {
        let fcm: FCMEventFormat = {
            title,
            body,
            data: {
                path: url
            },
            image
        };
        pushFcmNotification(sendTo, id, fcm);
    }

    public loginUser(req: Request, res: Response) {
        logger.debug("Login User", req.body);
        User.findOne({ user_name: req.body.user_name }, (err, user: IUser) => {
            if (err) {
                this.sendResponse({ success: false, message: err.message, error: err }, res);
            } else if (user == null) {
                this.sendResponse({ success: false, message: 'Invalid User.' }, res);
            } else if (user.password != req.body.password) {
                this.sendResponse({ success: false, message: 'Invalid Password.' }, res);
            } else {
                RoleBasedRoutes.findOne({ role: user.role }, (err, result) => {
                    if (err) {
                        this.sendResponse({ success: false, message: err.message, error: err }, res);
                    } else if (result == null) {
                        this.sendResponse({ success: false, message: 'Invalid User Role.' }, res);
                    } else {
                        const token = jwt.sign(
                            {
                                _id: user._id,
                                user_name: user.user_name,
                                role: user.role,
                                belongs_to: user.belongs_to,
                                company_name: user.company_name
                            },
                            config.jwtSecret,
                            { expiresIn: req.body.mobile ? '365d' : config.jwtExpiresIn }
                        );

                        let data = {
                            user: user,
                            routes: result.routes
                        };
                        if (user.role === AppConstants.ROLE_ADMIN) {
                            this.sendResponse({ success: true, data: data, token: token, message: 'Authentication Successful!' }, res);
                        } else {
                            if (user.activated === true) {
                                this.sendResponse({ success: true, data: data, token: token, message: 'Authentication Successful!' }, res);
                            } else {
                                this.sendResponse({ success: false, message: 'Your profile is not active. Please contact Administrator.' }, res);
                            }
                        }
                    }
                });
            }
        });
    }

    public addNewUser(req: Request, res: Response) {
        logger.debug("Add New User", req.body);
        let newUser = new User(req.body);

        newUser.save((err, user) => {
            if (err) {
                this.responseFormat = { success: false, message: err.message, error: err };
            } else {
                this.responseFormat = { success: true, message: 'User added Successfully!', data: user };
                this.socketResponse('New User created', 'users?userId=' + user._id, AppConstants.SOCKET_ROOM_ADMIN, undefined);
                this.pushFCM(
                    'New User created',
                    `User: ${user.first_name} ${user.last_name}.`,
                    `/users?id=${user._id}`,
                    AppConstants.ROLE_ADMIN,
                    undefined,
                    user.profile_pic_path
                );
            }
            return this.sendResponse(this.responseFormat, res);
        });
    }

    public async getAllUsers(req: Request, res: Response) {
        try {
            logger.debug("Get all User");
            let data = await User.find({ deleted: false }).sort({ _id: -1 }).exec();
            return this.sendResponse({ success: true, message: 'Request Success', data }, res);
        } catch (error) {
            logger.error('Get all User failed: ' + error);
            return this.sendResponse({ success: false, message: error.message || JSON.stringify(error) }, res);
        }
        // logger.debug("Get all User");
        // User.find({ deleted: false }, (err, users) => {
        //     if (err) {
        //         this.responseFormat = { success: false, message: err.message, error: err };
        //     } else {
        //         this.responseFormat = { success: true, message: 'Request Success', data: users };
        //     }
        //     return this.sendResponse(this.responseFormat, res);
        // });
    }

    public getUserWithID(req: Request, res: Response) {
        logger.debug("Get User With Id: " + req.params.userId);
        User.findById(req.params.userId, (err, user) => {
            if (err) {
                this.responseFormat = { success: false, message: err.message, error: err };
            } else {
                this.responseFormat = { success: true, message: 'Request Success', data: user };
            }
            return this.sendResponse(this.responseFormat, res);
        });
    }

    public updateUser(req: Request, res: Response) {
        logger.debug("Update User Id: " + req.params.userId + " Body", req.body);
        User.findOneAndUpdate({ _id: req.params.userId }, req.body, { new: true }, (err, user) => {
            if (err) {
                this.responseFormat = { success: false, message: err.message, error: err };
            } else {
                this.responseFormat = { success: true, data: user, message: 'User updated Successfully!' };
                this.socketResponse('User updated', 'users?userId=' + user._id, AppConstants.SOCKET_ROOM_ADMIN, undefined);
                this.pushFCM(
                    'User updated',
                    `User: ${user.first_name} ${user.last_name}.`,
                    `/users?id=${user._id}`,
                    AppConstants.ROLE_ADMIN,
                    user._id,
                    user.profile_pic_path
                );
                if (res.locals.jwtPayload.role == AppConstants.ROLE_ADMIN) {
                    this.socketResponse('Your Profile has been updated. Please relogin to continue.', '', undefined, user._id.toString());
                }
            }
            return this.sendResponse(this.responseFormat, res);
        });
    }

    public deleteUser(req: Request, res: Response) {
        logger.debug("Delete User With Id: " + req.params.userId);
        User.findByIdAndUpdate(req.params.userId, { deleted: true }, (err, user) => {
            if (err) {
                this.responseFormat = { success: false, message: err.message, error: err };
            } else {
                this.responseFormat = { success: true, message: 'User deleted Successfully!' };
                this.socketResponse('User deleted', 'users?userId=' + user._id, AppConstants.SOCKET_ROOM_ADMIN, undefined);
                this.socketResponse('Your Profile has been deleted. Please contact Administrator.', '', undefined, user._id.toString());
                this.pushFCM(
                    'User deleted',
                    `User: ${user.first_name} ${user.last_name}.`,
                    `/users?id=${user._id}`,
                    AppConstants.ROLE_ADMIN,
                    user._id,
                    user.profile_pic_path
                );
            }
            return this.sendResponse(this.responseFormat, res);
        });
    }

    public addRoleBasedRoutes(req: Request, res: Response) {
        logger.debug("Adding Role Based Routes data");
        RoleBasedRoutes.findOne({}, (err, result) => {
            if (err) {
                this.sendResponse({ success: false, message: err.message, error: err }, res);
            } else if (result) {
                this.sendResponse({ success: false, message: 'Roles already added.' }, res);
            } else {
                let roles1 = {
                    role: 'ADMIN',
                    routes: ['News Feed', 'My Profile', 'Clients', 'Users', 'All Bookings', 'Client Visitors', 'Events', 'Feedback/Suggestions', 'Achievers', 'Synerge Family']
                };
                let roles2 = {
                    role: 'CLIENT',
                    routes: ['News Feed', 'My Profile', 'My Bookings', 'My Visitors', 'Events', 'Feedback/Suggestions', 'Achievers', 'Synerge Family']
                };
                let roleBasedRoutes1 = new RoleBasedRoutes(roles1);
                let roleBasedRoutes2 = new RoleBasedRoutes(roles2);

                roleBasedRoutes1.save(() => { });
                roleBasedRoutes2.save(() => { });

                this.responseFormat = { success: true, message: 'Roles added Successfully!' };
                return this.sendResponse(this.responseFormat, res);
            }
        });
    }

    public sentMessageFromWebsite(req: Request, res: Response) {
        logger.debug("Sent message from website data: ", req.body);
        const { name, phone, email, message } = req.body;
        if (!name || !email || !message) {
            this.responseFormat = { success: false, message: 'Invalid Request' };
        } else {
            this.responseFormat = { success: true, message: 'Request success' };
        }

        return this.sendResponse(this.responseFormat, res);
    }

    public async resetPassword(req: Request, res: Response) {
        try {
            logger.debug("Reset password", req.body);
            if (!req.body.token || !req.body.password)
                return this.sendResponse({ success: false, message: 'Missing required fields' }, res);
            let jwtPayload = <any>jwt.verify(req.body.token, config.jwtEmailSecret);
            let user = await User.findOne({ _id: jwtPayload._id }).exec();
            if (!user) return this.sendResponse({ success: false, message: 'Unknown user' }, res);
            await User.findByIdAndUpdate(user._id, { password: req.body.password }).exec();
            return this.sendResponse({ success: true, message: 'Request Success' }, res);
        } catch (error) {
            logger.error('Reset password failed: ' + error);
            return this.sendResponse({ success: false, message: error.message || JSON.stringify(error) }, res);
        }
    }

}