import { Request, Response } from 'express';
import { logger } from '../lib/logger';
import { Clients } from '../models/clients';
import { ResponseFormat } from '../domains/response-format';

export class ClientController {

    public responseFormat: ResponseFormat;

    public sendResponse(result: ResponseFormat, res: Response) {
        res.json(result);
    }

    public addNewClient(req: Request, res: Response) {
        logger.debug("Add New Client", req.body);
        let newClient = new Clients(req.body);

        newClient.save((err, client) => {
            if (err) {
                this.responseFormat = { success: false, message: err.message, error: err };
            } else {
                this.responseFormat = { success: true, message: 'Clients added Successfully!', data: client };
            }
            return this.sendResponse(this.responseFormat, res);
        });
    }

    public async getAllClients(req: Request, res: Response) {
        try {
            logger.debug("Get all Client");
            let data = await Clients.find().sort({ _id: -1 }).exec();
            return this.sendResponse({ success: true, message: 'Request Success', data }, res);
        } catch (error) {
            logger.error('Get all Client failed: ' + error);
            return this.sendResponse({ success: false, message: error.message || JSON.stringify(error) }, res);
        }
        // logger.debug("Get all Client");
        // Clients.find((err, clients) => {
        //     if (err) {
        //         this.responseFormat = { success: false, message: err.message, error: err };
        //     } else {
        //         this.responseFormat = { success: true, message: 'Request Success', data: clients };
        //     }
        //     return this.sendResponse(this.responseFormat, res);
        // });
    }

    public getAllActiveClients(req: Request, res: Response) {
        logger.debug("Get all Active Client");
        Clients.find({ active_status: 'Active' }, (err, clients) => {
            if (err) {
                this.responseFormat = { success: false, message: err.message, error: err };
            } else {
                this.responseFormat = { success: true, message: 'Request Success', data: clients };
            }
            return this.sendResponse(this.responseFormat, res);
        });
    }

    public getClientWithID(req: Request, res: Response) {
        logger.debug("Get Client With Id: " + req.params.clientId);
        Clients.findById(req.params.clientId, (err, client) => {
            if (err) {
                this.responseFormat = { success: false, message: err.message, error: err };
            } else {
                this.responseFormat = { success: true, message: 'Request Success', data: client };
            }
            return this.sendResponse(this.responseFormat, res);
        });
    }

    public updateClient(req: Request, res: Response) {
        logger.debug("Update Client Id: " + req.params.clientId + " Body", req.body);
        Clients.findOneAndUpdate({ _id: req.params.clientId }, req.body, { new: true }, (err, client) => {
            if (err) {
                this.responseFormat = { success: false, message: err.message, error: err };
            } else {
                this.responseFormat = { success: true, data: client, message: 'Client updated Successfully!' };
            }
            return this.sendResponse(this.responseFormat, res);
        });
    }

    public deleteClient(req: Request, res: Response) {
        logger.debug("Delete Client With Id: " + req.params.clientId);
        Clients.findByIdAndDelete(req.params.clientId, (err) => {
            if (err) {
                this.responseFormat = { success: false, message: err.message, error: err };
            } else {
                this.responseFormat = { success: true, message: 'Client deleted Successfully!' };
            }
            return this.sendResponse(this.responseFormat, res);
        });
    }

}