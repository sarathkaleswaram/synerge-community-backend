import { Request, Response } from 'express';
import { logger } from '../lib/logger';
import { Achiever } from '../models/achievers';
import { FCMEventFormat, ResponseFormat } from '../domains/response-format';
import { pushFcmNotification } from '../lib/firebase-notification';

export class AchieverController {

    public responseFormat: ResponseFormat;

    public sendResponse(result: ResponseFormat, res: Response) {
        res.json(result);
    }

    public pushFCM(title: string, body: string, url: string, sendTo: string, id: string, image?: string) {
        let fcm: FCMEventFormat = {
            title,
            body,
            data: {
                path: url
            },
            image
        };
        pushFcmNotification(sendTo, id, fcm);
    }

    public addNewAchiever(req: Request, res: Response) {
        logger.debug("Add New Achiever", req.body);
        let newAchiever = new Achiever(req.body);

        newAchiever.save((err, achiever) => {
            if (err) {
                this.responseFormat = { success: false, message: err.message, error: err };
            } else {
                this.responseFormat = { success: true, message: 'Achiever added Successfully!', data: achiever };
                this.pushFCM(
                    'New Achiever added',
                    `Achiever: ${achiever.achiever}. Achieved: ${achiever.achieved}.`,
                    `/achievers?id=${achiever._id}`,
                    achiever.belongs_to,
                    undefined,
                    achiever.pic
                );
            }
            return this.sendResponse(this.responseFormat, res);
        });
    }

    public async getAllAchievers(req: Request, res: Response) {
        logger.debug("Get all Achiever");
        try {
            let achievers = await Achiever.find().sort({ _id: -1 }).exec();
            return this.sendResponse({ success: true, message: 'Request Success', data: achievers }, res);
        } catch (error) {
            logger.error('Get all Achiever failed: ' + error);
            return this.sendResponse({ success: false, message: error.message || JSON.stringify(error) }, res);
        }
    }

    public getAchieverWithID(req: Request, res: Response) {
        logger.debug("Get Achiever With Id: " + req.params.achieverId);
        Achiever.findById(req.params.achieverId, (err, achiever) => {
            if (err) {
                this.responseFormat = { success: false, message: err.message, error: err };
            } else {
                this.responseFormat = { success: true, message: 'Request Success', data: achiever };
            }
            return this.sendResponse(this.responseFormat, res);
        });
    }

    public updateAchiever(req: Request, res: Response) {
        logger.debug("Update Achiever Id: " + req.params.achieverId + " Body", req.body);
        Achiever.findOneAndUpdate({ _id: req.params.achieverId }, req.body, { new: true }, (err, achiever) => {
            if (err) {
                this.responseFormat = { success: false, message: err.message, error: err };
            } else {
                this.responseFormat = { success: true, data: achiever, message: 'Achiever updated Successfully!' };
                // this.pushFCM(
                //     'Achiever updated',
                //     `Achiever: ${achiever.achiever}. Achieved: ${achiever.achieved}.`,
                //     `/achievers?id=${achiever._id}`,
                //     achiever.belongs_to,
                //     undefined,
                //     achiever.pic
                // );
            }
            return this.sendResponse(this.responseFormat, res);
        });
    }

    public deleteAchiever(req: Request, res: Response) {
        logger.debug("Delete Achiever With Id: " + req.params.achieverId);
        Achiever.findByIdAndDelete(req.params.achieverId, (err) => {
            if (err) {
                this.responseFormat = { success: false, message: err.message, error: err };
            } else {
                this.responseFormat = { success: true, message: 'Achiever deleted Successfully!' };
            }
            return this.sendResponse(this.responseFormat, res);
        });
    }
}