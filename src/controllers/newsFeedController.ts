import { Request, Response } from 'express';
import { logger } from '../lib/logger';
import { NewsFeed, INewsFeed } from '../models/newsFeed';
import { ResponseFormat } from '../domains/response-format';

export class NewsFeedController {

    public responseFormat: ResponseFormat;

    public sendResponse(result: ResponseFormat, res: Response) {
        res.json(result);
    }

    public addNewsFeed(req: Request, res: Response) {
        logger.debug("Add New NewsFeed", req.body);
        let newsFeed: INewsFeed = new NewsFeed(req.body);

        newsFeed.save((err, newsfeed: INewsFeed) => {
            if (err) {
                this.responseFormat = { success: false, message: err.message, error: err };
            } else {
                this.responseFormat = { success: true, message: 'News feed added Successfully!', data: newsfeed };
            }
            return this.sendResponse(this.responseFormat, res);
        });
    }

    public async getAllNewsFeeds(req: Request, res: Response) {
        try {
            logger.debug("Get all NewsFeed");
            let data = await NewsFeed.find().sort({ _id: -1 }).exec();
            return this.sendResponse({ success: true, message: 'Request Success', data }, res);
        } catch (error) {
            logger.error('Get all NewsFeed failed: ' + error);
            return this.sendResponse({ success: false, message: error.message || JSON.stringify(error) }, res);
        }
        // logger.debug("Get all NewsFeed");
        // NewsFeed.find((err, newsfeeds: INewsFeed[]) => {
        //     if (err) {
        //         this.responseFormat = { success: false, message: err.message, error: err };
        //     } else {
        //         this.responseFormat = { success: true, message: 'Request Success', data: newsfeeds };
        //     }
        //     return this.sendResponse(this.responseFormat, res);
        // });
    }

    public getNewsFeedWithID(req: Request, res: Response) {
        logger.debug("Get NewsFeed With Id: " + req.params.newsFeedId);
        NewsFeed.findById(req.params.newsFeedId, (err, newsFeed) => {
            if (err) {
                this.responseFormat = { success: false, message: err.message, error: err };
            } else {
                this.responseFormat = { success: true, message: 'Request Success', data: newsFeed };
            }
            return this.sendResponse(this.responseFormat, res);
        });
    }

    public updateNewsFeed(req: Request, res: Response) {
        logger.debug("Update NewsFeed Id: " + req.params.newsFeedId + " Body", req.body);
        NewsFeed.findOneAndUpdate({ _id: req.params.newsFeedId }, req.body, { new: true }, (err, newsFeed) => {
            if (err) {
                this.responseFormat = { success: false, message: err.message, error: err };
            } else {
                this.responseFormat = { success: true, data: newsFeed, message: 'NewsFeed updated Successfully!' };
            }
            return this.sendResponse(this.responseFormat, res);
        });
    }

    public deleteNewsFeed(req: Request, res: Response) {
        logger.debug("Delete NewsFeed With Id: " + req.params.newsFeedId);
        NewsFeed.findByIdAndDelete(req.params.newsFeedId, (err) => {
            if (err) {
                this.responseFormat = { success: false, message: err.message, error: err };
            } else {
                this.responseFormat = { success: true, message: 'NewsFeed deleted Successfully!' };
            }
            return this.sendResponse(this.responseFormat, res);
        });
    }


}