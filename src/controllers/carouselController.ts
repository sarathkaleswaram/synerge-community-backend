import { Request, Response } from 'express';
import { logger } from '../lib/logger';
import { Carousel } from '../models/carousel';
import { FCMEventFormat, ResponseFormat } from '../domains/response-format';
import { pushFcmNotification } from '../lib/firebase-notification';

export class CarouselController {

    public responseFormat: ResponseFormat;

    public sendResponse(result: ResponseFormat, res: Response) {
        res.json(result);
    }

    public pushFCM(title: string, body: string, url: string, sendTo: string, id: string, image?: string) {
        let fcm: FCMEventFormat = {
            title,
            body,
            data: {
                path: url
            },
            image
        };
        pushFcmNotification(sendTo, id, fcm);
    }

    public addNewCarousel(req: Request, res: Response) {
        logger.debug("Add New Carousel", req.body);
        let newCarousel = new Carousel(req.body);

        newCarousel.save((err, carousel) => {
            if (err) {
                this.responseFormat = { success: false, message: err.message, error: err };
            } else {
                this.responseFormat = { success: true, message: 'Carousel added Successfully!', data: carousel };
                this.pushFCM('New Announcement created', ``, `/tabs/news-feed?carouselId=${carousel._id}`, 'SYNERGE_I', undefined, carousel.image_path);
                this.pushFCM('New Announcement created', ``, `/tabs/news-feed?carouselId=${carousel._id}`, 'SYNERGE_II', undefined, carousel.image_path);
            }
            return this.sendResponse(this.responseFormat, res);
        });
    }

    public async getAllCarousel(req: Request, res: Response) {
        try {
            logger.debug("Get all Carousel");
            let data = await Carousel.find().sort({ _id: -1 }).exec();
            return this.sendResponse({ success: true, message: 'Request Success', data }, res);
        } catch (error) {
            logger.error('Get all Carousel failed: ' + error);
            return this.sendResponse({ success: false, message: error.message || JSON.stringify(error) }, res);
        }
    }

    public getCarouselWithID(req: Request, res: Response) {
        logger.debug("Get Carousel With Id: " + req.params.carouselId);
        Carousel.findById(req.params.carouselId, (err, carousel) => {
            if (err) {
                this.responseFormat = { success: false, message: err.message, error: err };
            } else {
                this.responseFormat = { success: true, message: 'Request Success', data: carousel };
            }
            return this.sendResponse(this.responseFormat, res);
        });
    }

    public updateCarousel(req: Request, res: Response) {
        logger.debug("Update Carousel Id: " + req.params.carouselId + " Body", req.body);
        Carousel.findOneAndUpdate({ _id: req.params.carouselId }, req.body, { new: true }, (err, carousel) => {
            if (err) {
                this.responseFormat = { success: false, message: err.message, error: err };
            } else {
                this.responseFormat = { success: true, data: carousel, message: 'Carousel updated Successfully!' };
            }
            return this.sendResponse(this.responseFormat, res);
        });
    }

    public deleteCarousel(req: Request, res: Response) {
        logger.debug("Delete Carousel With Id: " + req.params.carouselId);
        Carousel.findByIdAndDelete(req.params.carouselId, (err, carousel) => {
            if (err) {
                this.responseFormat = { success: false, message: err.message, error: err };
            } else {
                this.responseFormat = { success: true, message: 'Carousel deleted Successfully!' };
            }
            return this.sendResponse(this.responseFormat, res);
        });
    }
}