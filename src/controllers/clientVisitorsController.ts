import { Request, Response } from 'express';
import * as moment from 'moment';
import { logger } from '../lib/logger';
import { ClientVisitors } from '../models/clientVisitors';
import { FCMEventFormat, ResponseFormat, SocketEventFormat } from '../domains/response-format';
import { AppConstants } from '../domains/app-constants';
import SynergeSocket from '../lib/socket';
import { pushFcmNotification } from '../lib/firebase-notification';

export class ClientVisitorsController {

    public responseFormat: ResponseFormat;

    public sendResponse(result: ResponseFormat, res: Response) {
        res.json(result);
    }

    public socketResponse(message: string, url: string, sendTo: string, id: string) {
        let socketResponse: SocketEventFormat = {
            type: AppConstants.TYPE_CLIENT_VISITOR,
            message: message,
            url: url
        };
        SynergeSocket.sendSocketResponse(socketResponse, sendTo, id);
    }

    public pushFCM(title: string, body: string, url: string, sendTo: string, id: string, image?: string) {
        let fcm: FCMEventFormat = {
            title,
            body,
            data: {
                path: url
            },
            image
        };
        pushFcmNotification(sendTo, id, fcm);
    }

    public addNewClientVisitor(req: Request, res: Response) {
        logger.debug("Add New ClientVisitors", req.body);
        let newClientVisitor = new ClientVisitors(req.body);

        newClientVisitor.save((err, clientVisitor) => {
            if (err) {
                this.responseFormat = { success: false, message: err.message, error: err };
            } else {
                this.responseFormat = { success: true, message: 'Client Visitor added Successfully!', data: clientVisitor };
                // this.socketResponse('New Client Visitor created', 'client-visitors?visitorId=' + clientVisitor._id, AppConstants.SOCKET_ROOM_ADMIN, undefined);
                this.socketResponse('New Client Visitor created', 'client-visitors?visitorId=' + clientVisitor._id, AppConstants.SOCKET_ROOM_ADMIN + clientVisitor.company_belongs_to, undefined);
                // this.socketResponse('New Client Visitor created', 'my-visitors?visitorId=' + clientVisitor._id, clientVisitor.visiting_company, undefined);
                this.socketResponse('New Client Visitor created', 'my-visitors?visitorId=' + clientVisitor._id, undefined, clientVisitor.whom_to_contact_id);
                this.pushFCM(
                    'New Visitor created',
                    `Visitor name: ${clientVisitor.visitor_name}, Time: ${moment.utc(clientVisitor.visiting_time).utcOffset("+05:30").format('DD-MM-YYYY h:mm A')}, Visiting Company: ${clientVisitor.visiting_company}, Purpose of Visit: ${clientVisitor.purpose_of_visit}`,
                    `/tabs/visitors?id=${clientVisitor._id}`,
                    AppConstants.ROLE_ADMIN + clientVisitor.company_belongs_to,
                    clientVisitor.whom_to_contact_id,
                    undefined
                );
            }
            return this.sendResponse(this.responseFormat, res);
        });
    }

    public async getAllClientVisitors(req: Request, res: Response) {
        try {
            let mongoQuery = {};
            if (res.locals.jwtPayload.role === AppConstants.ROLE_ADMIN) {
                logger.debug("Get all ClientVisitors");
                if (res.locals.jwtPayload.belongs_to)
                    mongoQuery['company_belongs_to'] = res.locals.jwtPayload.belongs_to;
            } else {
                logger.debug("Get my ClientVisitors");
                // mongoQuery['visiting_company'] = res.locals.jwtPayload.company_name;
                mongoQuery['whom_to_contact_id'] = res.locals.jwtPayload._id;
            }
            let data = await ClientVisitors.find(mongoQuery).sort({ _id: -1 }).exec();
            return this.sendResponse({ success: true, message: 'Request Success', data }, res);
        } catch (error) {
            logger.error('Get all ClientVisitors failed: ' + error);
            return this.sendResponse({ success: false, message: error.message || JSON.stringify(error) }, res);
        }
        // try {
        //     let mongoQuery = {};
        //     if (res.locals.jwtPayload.role === AppConstants.ROLE_ADMIN) {
        //         logger.debug("Get all ClientVisitors");
        //         if (res.locals.jwtPayload.belongs_to)
        //             mongoQuery['company_belongs_to'] = res.locals.jwtPayload.belongs_to;
        //     } else {
        //         logger.debug("Get all Company ClientVisitors");
        //         mongoQuery['visiting_company'] = res.locals.jwtPayload.company_name;
        //     }
        //     ClientVisitors.find(mongoQuery, (err, clientVisitors) => {
        //         if (err) {
        //             this.responseFormat = { success: false, message: err.message, error: err };
        //         } else {
        //             this.responseFormat = { success: true, message: 'Request Success', data: clientVisitors };
        //         }
        //         return this.sendResponse(this.responseFormat, res);
        //     });
        // } catch (error) {
        //     logger.error('Get all Account failed: ' + error);
        //     this.responseFormat = { success: false, message: error.message, error: error };
        //     return this.sendResponse(this.responseFormat, res);
        // }

        // if (res.locals.jwtPayload.role == AppConstants.ROLE_ADMIN) {
        //     logger.debug("Get all ClientVisitors");
        //     let mongoQuery = {};
        //     if (res.locals.jwtPayload.belongs_to)
        //         mongoQuery['company_belongs_to'] = res.locals.jwtPayload.belongs_to;
        //     ClientVisitors.find(mongoQuery, (err, clientVisitors) => {
        //         if (err) {
        //             this.responseFormat = { success: false, message: err.message, error: err };
        //         } else {
        //             this.responseFormat = { success: true, message: 'Request Success', data: clientVisitors };
        //         }
        //         return this.sendResponse(this.responseFormat, res);
        //     });
        // } else {
        //     logger.debug("Get all Company ClientVisitors");
        //     User.findById(res.locals.jwtPayload._id, async (err, user: IUser) => {
        //         if (err) {
        //             this.responseFormat = { success: false, message: err.message, error: err };
        //         } else {
        //             await ClientVisitors.find({ visiting_company: user.company_name }, (err, clientVisitors) => {
        //                 if (err) {
        //                     this.responseFormat = { success: false, message: err.message, error: err };
        //                 } else {
        //                     this.responseFormat = { success: true, message: 'Request Success', data: clientVisitors };
        //                 }
        //             });
        //         }
        //         return this.sendResponse(this.responseFormat, res);
        //     });
        // }
    }

    public getClientVisitorWithID(req: Request, res: Response) {
        logger.debug("Get ClientVisitors With Id: " + req.params.clientVisitorId);
        ClientVisitors.findById(req.params.clientVisitorId, (err, clientVisitor) => {
            if (err) {
                this.responseFormat = { success: false, message: err.message, error: err };
            } else {
                this.responseFormat = { success: true, message: 'Request Success', data: clientVisitor };
            }
            return this.sendResponse(this.responseFormat, res);
        });
    }

    public updateClientVisitor(req: Request, res: Response) {
        logger.debug("Update ClientVisitors Id: " + req.params.clientVisitorId + " Body", req.body);
        ClientVisitors.findOneAndUpdate({ _id: req.params.clientVisitorId }, req.body, { new: true }, (err, clientVisitor) => {
            if (err) {
                this.responseFormat = { success: false, message: err.message, error: err };
            } else {
                this.responseFormat = { success: true, data: clientVisitor, message: 'Client Visitor updated Successfully!' };
                // this.socketResponse('Client Visitor updated', 'client-visitors?visitorId=' + clientVisitor._id, AppConstants.SOCKET_ROOM_ADMIN, undefined);
                this.socketResponse('Client Visitor updated', 'client-visitors?visitorId=' + clientVisitor._id, AppConstants.SOCKET_ROOM_ADMIN + clientVisitor.company_belongs_to, undefined);
                this.socketResponse('Client Visitor updated', 'my-visitors?visitorId=' + clientVisitor._id, undefined, clientVisitor.whom_to_contact_id);
                this.pushFCM(
                    'Visitor updated',
                    `Visitor name: ${clientVisitor.visitor_name}, Time: ${moment.utc(clientVisitor.visiting_time).utcOffset("+05:30").format('DD-MM-YYYY h:mm A')}, Visiting Company: ${clientVisitor.visiting_company}, Purpose of Visit: ${clientVisitor.purpose_of_visit}`,
                    `/tabs/visitors?id=${clientVisitor._id}`,
                    AppConstants.ROLE_ADMIN + clientVisitor.company_belongs_to,
                    clientVisitor.whom_to_contact_id,
                    undefined
                );
            }
            return this.sendResponse(this.responseFormat, res);
        });
    }

    public deleteClientVisitor(req: Request, res: Response) {
        logger.debug("Delete ClientVisitors With Id: " + req.params.clientVisitorId);
        ClientVisitors.findByIdAndDelete(req.params.clientVisitorId, (err, clientVisitor) => {
            if (err) {
                this.responseFormat = { success: false, message: err.message, error: err };
            } else {
                this.responseFormat = { success: true, message: 'Client Visitor deleted Successfully!' };
                // this.socketResponse('Client Visitor deleted', 'client-visitors', AppConstants.SOCKET_ROOM_ADMIN, undefined);
                this.socketResponse('Client Visitor deleted', 'client-visitors', AppConstants.SOCKET_ROOM_ADMIN + clientVisitor.company_belongs_to, undefined);
                this.socketResponse('Client Visitor deleted', 'my-visitors', undefined, clientVisitor.whom_to_contact_id);
                this.pushFCM(
                    'Visitor deleted',
                    `Visitor name: ${clientVisitor.visitor_name}, Time: ${moment.utc(clientVisitor.visiting_time).utcOffset("+05:30").format('DD-MM-YYYY h:mm A')}, Visiting Company: ${clientVisitor.visiting_company}, Purpose of Visit: ${clientVisitor.purpose_of_visit}`,
                    `/tabs/visitors?id=${clientVisitor._id}`,
                    AppConstants.ROLE_ADMIN + clientVisitor.company_belongs_to,
                    clientVisitor.whom_to_contact_id,
                    undefined
                );
            }
            return this.sendResponse(this.responseFormat, res);
        });
    }

}