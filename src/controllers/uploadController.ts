import { Request, Response } from 'express';
import { logger } from '../lib/logger';
import { ResponseFormat } from '../domains/response-format';

export class UploadController {

    public responseFormat: ResponseFormat;

    public sendResponse(result: ResponseFormat, res: Response) {
        res.json(result);
    }

    public uploadFile(req: Request, res: Response) {
        logger.debug("Upload file", req.file);
        this.sendResponse({ success: true, message: 'success', data: req.file.filename }, res);
    }
}