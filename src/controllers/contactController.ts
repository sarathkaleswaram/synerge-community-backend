import { Request, Response } from 'express';
import { Contact } from '../models/contact';
// import { redisClient } from '../lib/redis';
import { RedisConstants } from '../domains/redis-constants';

export class ContactController {

    public addNewContact(req: Request, res: Response) {
        let newContact = new Contact(req.body);

        newContact.save((err, contact) => {
            if (err) {
                res.send(err);
            }
            res.json(contact);
        });
    }

    public getContacts(req: Request, res: Response) {
        // redisClient.get(RedisConstants.GET_ALL_USERS, (err, contact) => {
        //     if (contact) {
        //         return res.json({ source: 'cache', data: JSON.parse(contact) })
        //     } else {
        //         Contact.find({}, (err, contact) => {
        //             if (err) {
        //                 res.send(err);
        //             }
        //             redisClient.setex(RedisConstants.GET_ALL_USERS, 3600, JSON.stringify(contact));
        //             res.json({ source: 'api', data: contact });
        //         });
        //     }
        // });
    }

    public getContactWithID(req: Request, res: Response) {
        Contact.findById(req.params.contactId, (err, contact) => {
            if (err) {
                res.send(err);
            }
            res.json(contact);
        });
    }

    public updateContact(req: Request, res: Response) {
        Contact.findOneAndUpdate({ _id: req.params.contactId }, req.body, { new: true }, (err, contact) => {
            if (err) {
                res.send(err);
            }
            res.json(contact);
        });
    }

    public deleteContact(req: Request, res: Response) {
        Contact.remove({ _id: req.params.contactId }, (err) => {
            if (err) {
                res.send(err);
            }
            res.json({ message: 'Successfully deleted contact!' });
        });
    }

}