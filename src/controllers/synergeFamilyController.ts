import { Request, Response } from 'express';
import { logger } from '../lib/logger';
import { SynergeFamily } from '../models/synergeFamily';
import { ResponseFormat } from '../domains/response-format';

export class SynergeFamilyController {

    public responseFormat: ResponseFormat;

    public sendResponse(result: ResponseFormat, res: Response) {
        res.json(result);
    }

    public addNewSynergeFamily(req: Request, res: Response) {
        logger.debug("Add New SynergeFamily", req.body);
        let newSynergeFamily = new SynergeFamily(req.body);

        newSynergeFamily.save((err, synergeFamily) => {
            if (err) {
                this.responseFormat = { success: false, message: err.message, error: err };
            } else {
                this.responseFormat = { success: true, message: 'SynergeFamily added Successfully!', data: synergeFamily };
            }
            return this.sendResponse(this.responseFormat, res);
        });
    }

    public async getAllSynergeFamilys(req: Request, res: Response) {
        try {
            logger.debug("Get all SynergeFamily");
            let data = await SynergeFamily.find().sort({ _id: -1 }).exec();
            return this.sendResponse({ success: true, message: 'Request Success', data }, res);
        } catch (error) {
            logger.error('Get all SynergeFamily failed: ' + error);
            return this.sendResponse({ success: false, message: error.message || JSON.stringify(error) }, res);
        }
        // logger.debug("Get all SynergeFamily");
        // SynergeFamily.find((err, synergeFamilys) => {
        //     if (err) {
        //         this.responseFormat = { success: false, message: err.message, error: err };
        //     } else {
        //         this.responseFormat = { success: true, message: 'Request Success', data: synergeFamilys };
        //     }
        //     return this.sendResponse(this.responseFormat, res);
        // });
    }

    public getSynergeFamilyWithID(req: Request, res: Response) {
        logger.debug("Get SynergeFamily With Id: " + req.params.synergeFamilyId);
        SynergeFamily.findById(req.params.synergeFamilyId, (err, synergeFamily) => {
            if (err) {
                this.responseFormat = { success: false, message: err.message, error: err };
            } else {
                this.responseFormat = { success: true, message: 'Request Success', data: synergeFamily };
            }
            return this.sendResponse(this.responseFormat, res);
        });
    }

    public updateSynergeFamily(req: Request, res: Response) {
        logger.debug("Update SynergeFamily Id: " + req.params.synergeFamilyId + " Body", req.body);
        SynergeFamily.findOneAndUpdate({ _id: req.params.synergeFamilyId }, req.body, { new: true }, (err, synergeFamily) => {
            if (err) {
                this.responseFormat = { success: false, message: err.message, error: err };
            } else {
                this.responseFormat = { success: true, data: synergeFamily, message: 'SynergeFamily updated Successfully!' };
            }
            return this.sendResponse(this.responseFormat, res);
        });
    }

    public deleteSynergeFamily(req: Request, res: Response) {
        logger.debug("Delete SynergeFamily With Id: " + req.params.synergeFamilyId);
        SynergeFamily.findByIdAndDelete(req.params.synergeFamilyId, (err) => {
            if (err) {
                this.responseFormat = { success: false, message: err.message, error: err };
            } else {
                this.responseFormat = { success: true, message: 'SynergeFamily deleted Successfully!' };
            }
            return this.sendResponse(this.responseFormat, res);
        });
    }
}