import * as moment from 'moment';
import { AppConstants } from './../domains/app-constants';
import { Request, Response } from 'express';
import { logger } from '../lib/logger';
import { Bookings } from '../models/bookings';
import { FCMEventFormat, ResponseFormat, SocketEventFormat } from '../domains/response-format';
import SynergeSocket from '../lib/socket';
import { pushFcmNotification } from '../lib/firebase-notification';

export class BookingController {

    public responseFormat: ResponseFormat;

    public sendResponse(result: ResponseFormat, res: Response) {
        res.json(result);
    }

    public socketResponse(message: string, url: string, sendTo: string, id: string) {
        let socketResponse: SocketEventFormat = {
            type: AppConstants.TYPE_BOOKING,
            message: message,
            url: url
        };
        SynergeSocket.sendSocketResponse(socketResponse, sendTo, id);
    }

    public pushFCM(title: string, body: string, url: string, sendTo: string, id: string, image?: string) {
        let fcm: FCMEventFormat = {
            title,
            body,
            data: {
                path: url
            },
            image
        };
        pushFcmNotification(sendTo, id, fcm);
    }

    public addNewBooking(req: Request, res: Response) {
        logger.debug("Add New Bookings", req.body);
        let newBooking = new Bookings(req.body);

        newBooking.save((err, booking) => {
            if (err) {
                this.responseFormat = { success: false, message: err.message, error: err };
            } else {
                this.responseFormat = { success: true, message: 'Bookings added Successfully!', data: booking };
                // this.socketResponse('New Booking created', 'all-bookings?bookingId=' + booking._id, AppConstants.SOCKET_ROOM_ADMIN, undefined);
                this.socketResponse('New Booking created', 'all-bookings?bookingId=' + booking._id, AppConstants.SOCKET_ROOM_ADMIN + newBooking.belongs_to, undefined);
                this.pushFCM(
                    'New Booking created',
                    `Booking date: ${moment(newBooking.booking_date).add(1, 'd').format('DD-MM-YYYY')}, Time: ${newBooking.booking_time}`,
                    `/tabs/bookings?id=${newBooking._id}`,
                    AppConstants.ROLE_ADMIN + newBooking.belongs_to,
                    undefined,
                    undefined
                );
            }
            return this.sendResponse(this.responseFormat, res);
        });
    }

    public async getAllBookings(req: Request, res: Response) {
        try {
            logger.debug("Get all Bookings");
            let mongoQuery = {};
            if (res.locals.jwtPayload.belongs_to)
                mongoQuery['belongs_to'] = res.locals.jwtPayload.belongs_to;
            if (res.locals.jwtPayload.role !== AppConstants.ROLE_ADMIN)
                mongoQuery['by_user_id'] = res.locals.jwtPayload._id;
            let data = await Bookings.find(mongoQuery).sort({ _id: -1 }).exec();
            return this.sendResponse({ success: true, message: 'Request Success', data }, res);
        } catch (error) {
            logger.error('Get all Bookings failed: ' + error);
            return this.sendResponse({ success: false, message: error.message || JSON.stringify(error) }, res);
        }
        // if (res.locals.jwtPayload.role == AppConstants.ROLE_ADMIN) {
        //     logger.debug("Get all Bookings");
        //     let mongoQuery = {};
        //     if (res.locals.jwtPayload.belongs_to)
        //         mongoQuery['belongs_to'] = res.locals.jwtPayload.belongs_to;
        //     Bookings.find(mongoQuery, (err, bookings) => {
        //         if (err) {
        //             this.responseFormat = { success: false, message: err.message, error: err };
        //         } else {
        //             this.responseFormat = { success: true, message: 'Request Success', data: bookings };
        //         }
        //         return this.sendResponse(this.responseFormat, res);
        //     });
        // } else {
        //     logger.debug("Get all user Bookings");
        //     Bookings.find({ by_user_id: res.locals.jwtPayload._id }, (err, bookings) => {
        //         if (err) {
        //             this.responseFormat = { success: false, message: err.message, error: err };
        //         } else {
        //             this.responseFormat = { success: true, message: 'Request Success', data: bookings };
        //         }
        //         return this.sendResponse(this.responseFormat, res);
        //     });
        // }
    }

    public getBookingWithID(req: Request, res: Response) {
        logger.debug("Get Booking With Id: " + req.params.bookingId);
        Bookings.findById(req.params.bookingId, (err, booking) => {
            if (err) {
                this.responseFormat = { success: false, message: err.message, error: err };
            } else {
                this.responseFormat = { success: true, message: 'Request Success', data: booking };
            }
            return this.sendResponse(this.responseFormat, res);
        });
    }

    public updateBooking(req: Request, res: Response) {
        logger.debug("Update Booking Id: " + req.params.bookingId + " Body", req.body);
        Bookings.findOneAndUpdate({ _id: req.params.bookingId }, req.body, { new: true }, (err, booking) => {
            if (err) {
                this.responseFormat = { success: false, message: err.message, error: err };
            } else {
                this.responseFormat = { success: true, data: booking, message: 'Booking updated Successfully!' };
                // this.socketResponse('Booking updated', 'all-bookings?bookingId=' + booking._id, AppConstants.SOCKET_ROOM_ADMIN, undefined);
                this.socketResponse('Booking updated', 'all-bookings?bookingId=' + booking._id, AppConstants.SOCKET_ROOM_ADMIN + booking.belongs_to, undefined);
                this.socketResponse('Booking updated', 'my-booking?bookingId=' + booking._id, undefined, booking.by_user_id);
                this.pushFCM(
                    `Booking ${req.body.status}`,
                    `Booking date: ${moment(booking.booking_date).add(1, 'd').format('DD-MM-YYYY')}, Time: ${booking.booking_time}, Status: ${booking.status}`,
                    `/tabs/bookings?id=${booking._id}`,
                    AppConstants.ROLE_ADMIN + booking.belongs_to,
                    booking.by_user_id,
                    undefined
                );
            }
            return this.sendResponse(this.responseFormat, res);
        });
    }

    public deleteBooking(req: Request, res: Response) {
        logger.debug("Delete Booking With Id: " + req.params.bookingId);
        Bookings.findByIdAndDelete(req.params.bookingId, (err, booking) => {
            if (err) {
                this.responseFormat = { success: false, message: err.message, error: err };
            } else {
                this.responseFormat = { success: true, message: 'Booking deleted Successfully!' };
                // this.socketResponse('Booking deleted', 'all-bookings', AppConstants.SOCKET_ROOM_ADMIN, undefined);
                this.socketResponse('Booking deleted', 'all-bookings', AppConstants.SOCKET_ROOM_ADMIN + booking.belongs_to, undefined);
                this.socketResponse('Booking deleted', 'my-booking', undefined, booking.by_user_id);
                this.pushFCM(
                    'Booking Deleted',
                    `Booking date: ${moment(booking.booking_date).add(1, 'd').format('DD-MM-YYYY')}, Time: ${booking.booking_time}`,
                    `/tabs/bookings?id=${booking._id}`,
                    AppConstants.ROLE_ADMIN + booking.belongs_to,
                    booking.by_user_id,
                    undefined
                );
            }
            return this.sendResponse(this.responseFormat, res);
        });
    }

    public getBookingAvailableSlots(req: Request, res: Response) {
        logger.debug("Get Booking Available Slots", req.body);
        // Bookings.find({ belongs_to: req.body.belongs_to, $or: [{ status: 'Confirmed' }, { status: 'Request Pending' }], booking_data: { $elemMatch: { date: req.body.date } } }, (err, booking) => {
        Bookings.find({
            belongs_to: req.body.belongs_to,
            booking_type: req.body.booking_type,
            booking_date: req.body.booking_date,
            $or: [{ status: 'Confirmed' }, { status: 'Request Pending' }]
        },
            (err, booking) => {
                if (err) {
                    this.responseFormat = { success: false, message: err.message, error: err };
                } else {
                    this.responseFormat = { success: true, message: 'Request Success', data: booking };
                }
                return this.sendResponse(this.responseFormat, res);
            });
    }

}