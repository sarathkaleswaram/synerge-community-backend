import * as express from 'express';
import * as bodyParser from 'body-parser';
import * as mongoose from 'mongoose';
import * as compression from 'compression';
import * as morgan from 'morgan';
import * as yamlConfig from 'yaml-config';
import * as cors from 'cors';
import * as favicon from 'serve-favicon';
import * as path from 'path';
import * as socket from 'socket.io';
import * as webpush from 'web-push';
import { CronJob } from 'cron';
import { Routes } from './routes';
import { logger } from './lib/logger';
import SynergeSocket from './lib/socket';
import { updateCompletedStatus, sendGoodMorning } from './lib/cron-job';

export const config = yamlConfig.readConfig('config.yml');
export let io: SocketIO.Server;

class App {

    public app: express.Application = express();
    private route: Routes = new Routes();
    private mongoUrl: string = 'mongodb://';

    constructor() {
        this.config();
        this.mongoSetup();
        this.startCronJob();
        this.route.routes(this.app);
        // web push notification
        webpush.setVapidDetails('mailto:synergeworkspace@gmail.com', config.webPush.publicKey, config.webPush.privateKey);
    }

    private config(): void {
        this.app.use(cors());
        this.app.use(morgan('dev', {
            skip: (req) => { return req.originalUrl.includes('favicon.svg') }
        }));
        this.app.use(compression());
        this.app.use(bodyParser.json());
        this.app.use(bodyParser.urlencoded({ extended: false }));
        this.app.use(express.static(path.join(__dirname, '..', 'public')));
        this.app.use(favicon(path.join(__dirname, '..', 'public', 'favicon.ico')));
    }

    private mongoSetup(): void {
        if (config.mongodb.authentication) {
            this.mongoUrl = this.mongoUrl + config.mongodb.credentials.user + ':' + config.mongodb.credentials.password + '@';
        }
        this.mongoUrl = this.mongoUrl + config.mongodb.host + ':' + config.mongodb.port + '/' + config.mongodb.db;

        mongoose.connect(this.mongoUrl, { useNewUrlParser: true, useCreateIndex: true, useUnifiedTopology: true, useFindAndModify: false });
        mongoose.connection.on('connected', () => {
            logger.info('MongoDB connected.');
        });
    }

    public socketSetup(server) {
        io = socket().listen(server);
        SynergeSocket.handleSocket();
    }

    private startCronJob() {
        // runs 11:55 PM - 55 23 * * *
        // every 5 secs - */5 * * * * *
        let job = new CronJob('55 23 * * *', () => {
            logger.info('Running Cron Job.');
            updateCompletedStatus();
        }, null, true, 'Asia/Kolkata');
        job.start();
        // send good morning notification at 9 AM - 0 9 * * *
        let goodMorningCron = new CronJob('0 9 * * *', () => {
            logger.info('Running Cron Job for Good morning.');
            sendGoodMorning();
        }, null, true, 'Asia/Kolkata');
        goodMorningCron.start();
    }
}

export default new App();
