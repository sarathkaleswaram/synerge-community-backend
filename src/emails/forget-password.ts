export default (name: string, url: string) => {
  return `
  <!DOCTYPE html
      PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml"
      xmlns:o="urn:schemas-microsoft-com:office:office">

    <head>
      <!--[if gte mso 9]>
                <xml>
                  <o:OfficeDocumentSettings>
                    <o:AllowPNG/>
                    <o:PixelsPerInch>96</o:PixelsPerInch>
                  </o:OfficeDocumentSettings>
                </xml>
                <![endif]-->
      <!--[if !mso]><!-->
      <link
        href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap"
        rel="stylesheet" />
      <!--<![endif]-->
      <title>Testd</title>
      <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
      <meta http-equiv="X-UA-Compatible" content="IE=edge" />
      <style type="text/css">
        table {
          border-collapse: collapse;
          mso-table-lspace: 0px;
          mso-table-rspace: 0px;
        }

        td,
        a,
        span {
          border-collapse: collapse;
          mso-line-height-rule: exactly;
        }

        p {
          padding: 0 !important;
          margin: 0 !important;
        }

        h1,
        h2,
        h3,
        h4,
        h5,
        h6 {
          display: block;
          margin: 0;
        }

        img {
          border: 0;
          outline: none;
          text-decoration: none;
        }

        p,
        a,
        li,
        td,
        blockquote {
          mso-line-height-rule: exactly;
        }

        p,
        a,
        li,
        td,
        body,
        table,
        blockquote {
          -ms-text-size-adjust: 100%;
          -webkit-text-size-adjust: 100%;
        }

        a {
          color: inherit;
          text-decoration: none;
        }

        .qe_black a {
          color: #939598;
          text-decoration: none;
        }

        .qe_white a {
          color: #ffffff;
          text-decoration: none;
        }

        .qe_blue a {
          text-decoration: none;
          color: #0F84A9;
        }

        .qe_grey1 a {
          text-decoration: none;
          color: #6D6E71;
        }

        /*assets css start end*/
        body {
          margin: 0 !important;
          padding: 0 !important;
          -webkit-text-size-adjust: 100% !important;
          -ms-text-size-adjust: 100% !important;
          -webkit-font-smoothing: antialiased !important;
        }

        img+div {
          display: none;
        }

        .button {
          background-color: #b80203;
          /* Green */
          border: none;
          color: white;
          padding: 15px 32px;
          text-align: center;
          text-decoration: none;
          display: inline-block;
          font-size: 16px;
          margin: 4px 2px;
          cursor: pointer;
          -webkit-transition-duration: 0.4s;
          /* Safari */
          transition-duration: 0.4s;
        }

        .button1 {
          box-shadow: 0 8px 16px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
        }

        @media only screen and (min-width: 480px) and (max-width: 599px) {
          .qe_wrapper {
            max-width: 100% !important;
            width: 100% !important;
            float: none !important;
          }

          .qe_img100 {
            width: 100% !important;
            height: auto !important;
            max-width: 100% !important;
          }

          .qe_w10 {
            width: 10px !important;
          }

          .qe_top20 {
            padding-top: 20px !important;
          }

          .qe_none {
            display: none !important;
          }

          .qe_pad_all {
            padding: 20px 30px !important;
          }
        }

        @media only screen and (max-width: 479px) {
          .qe_wrapper {
            max-width: 100% !important;
            width: 100% !important;
            float: none !important;
          }

          .qe_img100 {
            width: 100% !important;
            height: auto !important;
            max-width: 100% !important;
          }

          .qe_w10 {
            width: 10px !important;
          }

          .qe_top20 {
            padding-top: 20px !important;
          }

          .qe_none {
            display: none !important;
          }

          .qe_pad_all {
            padding: 20px 30px !important;
          }

          u+.body .full-wrap {
            width: 100% !important;
            width: 100vw !important;
          }
        }
      </style>
    </head>

    <body class="body"
      style="padding:0; margin:0 auto !important; display:block !important; min-width:100% !important; width:100% !important; background:#ffffff; -webkit-text-size-adjust:none">
      <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff" class="full-wrap">
        <tr>
          <td align="center" valign="top">
            <table align="center" style="width:600px; max-width:600px; table-layout:fixed;" class="qe_wrapper" width="600"
              border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td align="center" valign="top">
                  <!-- Content Section Start -->

                  <table width="600" border="0" cellspacing="0" cellpadding="0" class="qe_wrapper" align="center"
                    style="width:600px;">
                    <tr>
                      <td align="center" valign="top" style="padding:10px 20px 20px;" class="qe_pad_all">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                          <tr>
                            <td class="qe_grey1" align="center" valign="top"
                              style="font:600 15px/18px 'Montserrat', Arial, sans-serif; color:#b80203; letter-spacing:1px;">
                              Dear ${name},</td>
                          </tr>
                          <tr>
                            <td class="qe_blue" align="center" valign="top"
                              style="font:600 16px/20px 'Montserrat', Arial, sans-serif; padding-top:5px; color:#6D6E71;">
                              Click below link to Reset your password.</td>
                          </tr>
                          <tr>
                            <td class="qe_top20 qe_blue" align="center" valign="top"
                              style="font:600 16px/19px 'Montserrat', Arial, sans-serif; padding-top:24px; color:#b80203;">
                              <button class="button button1">
                              <a href="${url}" target="_blank">Click Here</a>
                              </button>
                            </td>
                          </tr>
                          <tr>
                            <td class="qe_blue" align="center" valign="top"
                              style="font:600 12px/16px 'Montserrat', Arial, sans-serif; padding-top:5px; color:#6D6E71;">
                              This link will expire in 1 hour.</td>
                          </tr>
                          <tr>
                            <td align="center" valign="top" style="padding-top:35px;" class="qe_top20">
                              <table width="432" border="0" cellspacing="0" cellpadding="0" align="center"
                                style="width:432px;" class="qe_wrapper">
                                <tr>
                                  <td height="1" style="height:1px; line-height:0px; font-size:0px;" bgcolor="#dcdedf"><img
                                      src="images/default_img.gif" width="1" height="1" alt="" border="0"
                                      style="display:block; " /></td>
                                </tr>
                              </table>
                            </td>
                          </tr>
                          
                          <tr>
                            <td class="qe_top20 qe_black" align="center" valign="top"
                              style="font:11px/16px 'Montserrat', Arial, sans-serif; color:#6D6E71; padding-top:10px;"><a
                                href="#" target="_blank"
                                style="text-decoration:none; color:#6D6E71; white-space:nowrap;">Synerge Workspace</a>&nbsp;|&nbsp;<a href="#" target="_blank"
                                style="text-decoration:none; color:#6D6E71;">Community</a>&nbsp;</td>
                          </tr>
                        </table>
                      </td>
                    </tr>
                  </table>

                  <!-- Footer Section End -->
                </td>
              </tr>
              <tr>
                <td valign="top" height="1" class="qe_none" style="height:1px; line-height:0px; font-size:0px;"><img
                    src="images/default_img.gif" width="600" height="1" alt="" border="0"
                    style="display:block; max-width:600px;" /></td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </body>

    </html>
  `;
}