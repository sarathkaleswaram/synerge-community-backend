
import { Request, Response, NextFunction } from 'express';
import * as jwt from 'jsonwebtoken';
import { config } from '../app';
import { logger } from '../lib/logger';
import { AppConstants } from '../domains/app-constants';
import { ResponseFormat } from '../domains/response-format';

export const checkJwt = (req: Request, res: Response, next: NextFunction) => {
  const token = <string>req.headers['x-jwt-auth'];

  try {
    let jwtPayload = <any>jwt.verify(token, config.jwtSecret);
    res.locals.jwtPayload = jwtPayload;
    // logger.debug('JWT Data', jwtPayload);
  } catch (error) {
    logger.error(error);
    let responseFormat: ResponseFormat = { success: false, message: 'Invalid Session or Session expired.' };
    res.status(401).json(responseFormat);
    return;
  }

  next();
};

export const verifyJwt = (token: string) => {
  try {
    return <any>jwt.verify(token, config.jwtSecret);
  } catch (error) {
    logger.error(error);
    return null;
  }
}

export const authUser = (req: Request, res: Response, next: NextFunction) => {
  if (res.locals.jwtPayload.role == AppConstants.ROLE_ADMIN) {
    next();
  } else if (req.params.userId == res.locals.jwtPayload._id) {
    next();
  } else {
    logger.warn('Unauthorised', res.locals.jwtPayload);
    res.status(401).json({ success: false, message: 'You are not authorised to do this action.' });
  }
};