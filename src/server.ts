import * as https from 'https';
import * as http from 'http';
import * as fs from 'fs';
import App, { config } from './app';
import { logger } from './lib/logger';

if (config.sslEnabled) {
    const httpsOptions = {
        key: fs.readFileSync('./certs/key.pem'),
        cert: fs.readFileSync('./certs/cert.pem')
    }
    let server = https.createServer(httpsOptions, App.app).listen(config.port, () => {
        logger.info('Server listening on port ' + config.port);
        App.socketSetup(server);
    })
} else {
    let server = http.createServer(App.app).listen(config.port, () => {
        logger.info('Server listening on port: ' + config.port);
        App.socketSetup(server);
    });
}