import { Request, Response, Application } from 'express';
import { registerUserSW } from '../lib/webPush';
import { registerUserToken } from '../lib/firebase-notification';
import { checkJwt, authUser } from '../middleware';
import { ResponseFormat } from '../domains/response-format';
import { upload } from '../lib/upload';
import { ContactController } from '../controllers/contactController';
import { UserController } from '../controllers/userController';
import { UploadController } from '../controllers/uploadController';
import { NewsFeedController } from '../controllers/newsFeedController';
import { CarouselController } from '../controllers/carouselController';
import { BookingController } from '../controllers/bookingController';
import { ClientController } from '../controllers/clientsController';
import { ClientVisitorsController } from '../controllers/clientVisitorsController';
import { EventController } from '../controllers/eventController';
import { FeedbackController } from '../controllers/feedbackController';
import { SynergeFamilyController } from '../controllers/synergeFamilyController';
import { AchieverController } from '../controllers/achieverController';
import { sendForgotPassword } from '../lib/mail';

export class Routes {

    public contactController: ContactController = new ContactController();
    public userController: UserController = new UserController();
    public uploadController: UploadController = new UploadController();
    public newsFeedController: NewsFeedController = new NewsFeedController();
    public carouselController: CarouselController = new CarouselController();
    public bookingController: BookingController = new BookingController();
    public clientController: ClientController = new ClientController();
    public clientVisitorsController: ClientVisitorsController = new ClientVisitorsController();
    public eventController: EventController = new EventController();
    public feedbackController: FeedbackController = new FeedbackController();
    public synergeFamilyController: SynergeFamilyController = new SynergeFamilyController();
    public achieverController: AchieverController = new AchieverController();
    public responseFormat: ResponseFormat;

    public routes(app: Application): void {

        app.route('/')
            .get((req, res) => this.sendSuccess(req, res))

        // push notification - desktop notification
        app.route('/pn-subscription')
            .post(checkJwt, (req, res) => registerUserSW(req, res));

        // firebase cloud messaging - mobile notification
        app.route('/fcm-subscription')
            .post(checkJwt, (req, res) => registerUserToken(req, res));

        // User
        app.route('/login')
            .post((req, res) => this.userController.loginUser(req, res));

        app.route('/forgot-password')
            .post((req, res) => sendForgotPassword(req, res));

        app.route('/reset-password')
            .post((req, res) => this.userController.resetPassword(req, res));

        app.route('/user')
            .post((req, res) => this.userController.addNewUser(req, res))
            .get(checkJwt, authUser, (req, res) => this.userController.getAllUsers(req, res));

        app.route('/user/:userId')
            .get(checkJwt, (req, res) => this.userController.getUserWithID(req, res))
            .put(checkJwt, authUser, (req, res) => this.userController.updateUser(req, res))
            .delete(checkJwt, authUser, (req, res) => this.userController.deleteUser(req, res));

        // Upload
        app.route('/upload')
            .post(checkJwt, upload.single('uploadImage'), (req, res) => this.uploadController.uploadFile(req, res));

        // News Feed
        app.route('/news-feed')
            .post(checkJwt, (req, res) => this.newsFeedController.addNewsFeed(req, res))
            .get(checkJwt, (req, res) => this.newsFeedController.getAllNewsFeeds(req, res));

        app.route('/news-feed/:newsFeedId')
            .get(checkJwt, (req, res) => this.newsFeedController.getNewsFeedWithID(req, res))
            .put(checkJwt, (req, res) => this.newsFeedController.updateNewsFeed(req, res))
            .delete(checkJwt, authUser, (req, res) => this.newsFeedController.deleteNewsFeed(req, res));

        // Carousel
        app.route('/carousel')
            .post(checkJwt, (req, res) => this.carouselController.addNewCarousel(req, res))
            .get(checkJwt, (req, res) => this.carouselController.getAllCarousel(req, res));

        app.route('/carousel/:carouselId')
            .get(checkJwt, (req, res) => this.carouselController.getCarouselWithID(req, res))
            .put(checkJwt, (req, res) => this.carouselController.updateCarousel(req, res))
            .delete(checkJwt, authUser, (req, res) => this.carouselController.deleteCarousel(req, res));

        // Bookings
        app.route('/booking')
            .post(checkJwt, (req, res) => this.bookingController.addNewBooking(req, res))
            .get(checkJwt, (req, res) => this.bookingController.getAllBookings(req, res));

        app.route('/booking/:bookingId')
            .get(checkJwt, (req, res) => this.bookingController.getBookingWithID(req, res))
            .put(checkJwt, (req, res) => this.bookingController.updateBooking(req, res))
            .delete(checkJwt, authUser, (req, res) => this.bookingController.deleteBooking(req, res));

        app.route('/bookingAvailableSlots')
            .post(checkJwt, (req, res) => this.bookingController.getBookingAvailableSlots(req, res));

        // Clients
        app.route('/client')
            .post(checkJwt, authUser, (req, res) => this.clientController.addNewClient(req, res))
            .get(checkJwt, authUser, (req, res) => this.clientController.getAllClients(req, res));

        app.route('/client/active')
            .get(checkJwt, authUser, (req, res) => this.clientController.getAllActiveClients(req, res));

        app.route('/client/:clientId')
            .get(checkJwt, authUser, (req, res) => this.clientController.getClientWithID(req, res))
            .put(checkJwt, authUser, (req, res) => this.clientController.updateClient(req, res))
            .delete(checkJwt, authUser, (req, res) => this.clientController.deleteClient(req, res));

        // Client Visitors
        app.route('/client-visitors')
            .post(checkJwt, authUser, (req, res) => this.clientVisitorsController.addNewClientVisitor(req, res))
            .get(checkJwt, (req, res) => this.clientVisitorsController.getAllClientVisitors(req, res));

        app.route('/client-visitors/:clientVisitorId')
            .get(checkJwt, (req, res) => this.clientVisitorsController.getClientVisitorWithID(req, res))
            .put(checkJwt, authUser, (req, res) => this.clientVisitorsController.updateClientVisitor(req, res))
            .delete(checkJwt, authUser, (req, res) => this.clientVisitorsController.deleteClientVisitor(req, res));

        // Events
        app.route('/event')
            .post(checkJwt, authUser, (req, res) => this.eventController.addNewEvent(req, res))
            .get(checkJwt, (req, res) => this.eventController.getAllEvents(req, res));

        app.route('/event/:eventId')
            .get(checkJwt, (req, res) => this.eventController.getEventWithID(req, res))
            .put(checkJwt, (req, res) => this.eventController.updateEvent(req, res))
            .delete(checkJwt, authUser, (req, res) => this.eventController.deleteEvent(req, res));

        // Feedback
        app.route('/feedback')
            .post(checkJwt, (req, res) => this.feedbackController.addNewFeedback(req, res))
            .get(checkJwt, (req, res) => this.feedbackController.getAllFeedback(req, res));

        app.route('/feedback/:feedbackId')
            .get(checkJwt, (req, res) => this.feedbackController.getFeedbackWithID(req, res))
            .put(checkJwt, (req, res) => this.feedbackController.updateFeedback(req, res))
            .delete(checkJwt, (req, res) => this.feedbackController.deleteFeedback(req, res));

        // SynergeFamily
        app.route('/synerge-family')
            .post(checkJwt, authUser, (req, res) => this.synergeFamilyController.addNewSynergeFamily(req, res))
            .get(checkJwt, (req, res) => this.synergeFamilyController.getAllSynergeFamilys(req, res));

        app.route('/synerge-family/:synergeFamilyId')
            .get(checkJwt, (req, res) => this.synergeFamilyController.getSynergeFamilyWithID(req, res))
            .put(checkJwt, authUser, (req, res) => this.synergeFamilyController.updateSynergeFamily(req, res))
            .delete(checkJwt, authUser, (req, res) => this.synergeFamilyController.deleteSynergeFamily(req, res));

        // Achievers
        app.route('/achiever')
            .post(checkJwt, authUser, (req, res) => this.achieverController.addNewAchiever(req, res))
            .get(checkJwt, (req, res) => this.achieverController.getAllAchievers(req, res));

        app.route('/achiever/:achieverId')
            .get(checkJwt, (req, res) => this.achieverController.getAchieverWithID(req, res))
            .put(checkJwt, authUser, (req, res) => this.achieverController.updateAchiever(req, res))
            .delete(checkJwt, authUser, (req, res) => this.achieverController.deleteAchiever(req, res));

        // Contact 
        app.route('/contact')
            .get(this.contactController.getContacts)
            .post(checkJwt, this.contactController.addNewContact);
        // Contact detail
        app.route('/contact/:contactId')
            .get(checkJwt, this.contactController.getContactWithID)
            .put(checkJwt, this.contactController.updateContact)
            .delete(checkJwt, this.contactController.deleteContact);

        // Roles
        app.route('/roles')
            .get((req, res) => this.userController.addRoleBasedRoutes(req, res));

        // Website message
        app.route('/send-message')
            .post((req, res) => this.userController.sentMessageFromWebsite(req, res));

        app.route('*').get((req, res) => this.send404(req, res));
        app.route('*').post((req, res) => this.send404(req, res));
        app.route('*').put((req, res) => this.send404(req, res));
        app.route('*').delete((req, res) => this.send404(req, res));
    }

    sendSuccess(req: Request, res: Response) {
        this.responseFormat = {
            success: true,
            message: 'Synerge Backend API Running!'
        };
        res.setHeader('Content-Type', 'application/json');
        res.status(200).send(JSON.stringify(this.responseFormat, undefined, 2));
    }

    send404(req: Request, res: Response) {
        this.responseFormat = {
            success: false,
            message: 'Cannot ' + req.method + ' ' + req.originalUrl
        };
        res.setHeader('Content-Type', 'application/json');
        res.status(404).send(JSON.stringify(this.responseFormat, undefined, 2));
    }
}