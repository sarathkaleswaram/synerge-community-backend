import * as mongoose from 'mongoose';

type RoleEnum = 'ADMIN' | 'CLIENT';
type BelongsToEnum = 'SYNERGE_I' | 'SYNERGE_II';

interface keys {
  p256dh: string,
  auth: string
}

export interface INotificationKeys extends mongoose.Document {
  endpoint: string,
  keys: keys,
  createDate: Date,
  user_id: string,
  role: RoleEnum;
  belongs_to?: BelongsToEnum;
  company_name?: string;
};

const NotificationKeysSchema = new mongoose.Schema({
  endpoint: String,
  keys: mongoose.Schema.Types.Mixed,
  createDate: { type: Date, default: Date.now },
  user_id: { type: String, required: true },
  role: { type: String, required: true },
  belongs_to: { type: String },
  company_name: { type: String }
});

export const NotificationKeys = mongoose.model<INotificationKeys>('NotificationKeys', NotificationKeysSchema);