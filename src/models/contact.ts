import * as mongoose from 'mongoose';

const ContactSchema = new mongoose.Schema({
    name: String,
    username: { type: String, required: true, unique: true },
    password: { type: String, required: true },
    admin: Boolean,
    location: String,
    meta: {
      age: Number,
      website: String
    },
    created_at: Date,
    updated_at: Date
});

export const Contact = mongoose.model('Contact', ContactSchema);