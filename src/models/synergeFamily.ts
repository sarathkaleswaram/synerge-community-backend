import * as mongoose from 'mongoose';

type BelongsToEnum = 'SYNERGE_I' | 'SYNERGE_II';

interface CompanyHead {
  designation: string;
  name: string;
};

export interface ISynergeFamily extends mongoose.Document {
  belongs_to: BelongsToEnum;
  company_name: string;
  company_description: string;
  company_link: string;
  company_logo_path: string;
  company_heads: CompanyHead[];
};

const SynergeFamilySchema = new mongoose.Schema({
  belongs_to: { type: String, required: true },
  company_name: { type: String, required: true },
  company_description: { type: String },
  company_link: { type: String },
  company_logo_path: { type: String },
  company_heads: { type: Array }
});

export const SynergeFamily = mongoose.model<ISynergeFamily>('SynergeFamily', SynergeFamilySchema);