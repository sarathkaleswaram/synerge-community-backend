import * as mongoose from 'mongoose';

type FeedbackStatusEnum = 'Not Seen' | 'On Progress' | 'Completed';
type BelongsToEnum = 'SYNERGE_I' | 'SYNERGE_II';

interface Comments {
    comment: string;
    by_user_id: string;
    by_user_name: string;
    date: Date;
    reply?: Comments;
};

export interface IFeedback extends mongoose.Document {
    by_user_id: string;
    by_user_name: string;
    feedback: string;
    requested_date: Date;
    belongs_to: BelongsToEnum;
    company_name: string;
    comments: Comments[];
    status: FeedbackStatusEnum;
};

const FeedbackSchema = new mongoose.Schema({
    by_user_id: { type: String, required: true },
    by_user_name: { type: String, required: true },
    feedback: { type: String, required: true },
    requested_date: { type: Date, required: true },
    belongs_to: { type: String, required: true },
    company_name: { type: String, required: true },
    comments: { type: Array },
    status: { type: String, required: true },
});

export const Feedback = mongoose.model<IFeedback>('Feedback', FeedbackSchema);