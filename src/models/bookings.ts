import * as mongoose from 'mongoose';

type StatusEnum = 'Request Pending' | 'Confirmed' | 'Declined' | 'Completed';
type BookingTypeEnum = 'Discussion room' | 'Conference room' | 'Pantry' | 'Board room' | 'Game room';
type BelongsToEnum = 'SYNERGE_I' | 'SYNERGE_II';

export interface IBookings extends mongoose.Document {
    by_user_id: string;
    by_user_name: string;
    // requested_date: Date;
    booking_created_date: Date;
    belongs_to: BelongsToEnum;
    company_name: string;
    // booking_data: any[];    
    booking_type: BookingTypeEnum;
    booking_date: Date;
    booking_time: string;
    status: StatusEnum;
    booking_note?: string;
};

const BookingsSchema = new mongoose.Schema({
    by_user_id: { type: String, required: true },
    by_user_name: { type: String, required: true },
    // requested_date: { type: Date, required: true },
    booking_created_date: { type: Date, required: true },
    belongs_to: { type: String, required: true },
    company_name: { type: String, required: true },
    // booking_data: { type: Array, required: true },
    booking_type: { type: String, required: true },
    booking_date: { type: Date, required: true },
    booking_time: { type: String, required: true },
    status: { type: String, required: true },
    booking_note: { type: String }
});

export const Bookings = mongoose.model<IBookings>('Bookings', BookingsSchema);