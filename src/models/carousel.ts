import * as mongoose from 'mongoose';

export interface ICarousel extends mongoose.Document {
    by_user_id: string;
    by_user_name: string;
    date: Date;
    image_path: string;
};

const CarouselSchema = new mongoose.Schema({
    by_user_id: { type: String, required: true },
    by_user_name: { type: String, required: true },
    date: { type: Date, required: true, default: new Date() },
    image_path: { type: String, required: true },
});

export const Carousel = mongoose.model<ICarousel>('Carousel', CarouselSchema);