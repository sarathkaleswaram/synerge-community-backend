import * as mongoose from 'mongoose';

type BelongsToEnum = 'SYNERGE_I' | 'SYNERGE_II';

export interface IClientVisitors extends mongoose.Document {
    visitor_name: string;
    visitor_phone: string;
    visiting_time: Date;
    purpose_of_visit: string;
    whom_to_contact: string;
    whom_to_contact_id: string;
    visiting_company: string;
    company_belongs_to: BelongsToEnum;
};

const ClientVisitorsSchema = new mongoose.Schema({
    visitor_name: { type: String, required: true },
    visitor_phone: { type: String, required: true },
    visiting_time: { type: Date, required: true },
    purpose_of_visit: { type: String, required: true },
    whom_to_contact: { type: String, required: true },
    whom_to_contact_id: { type: String, required: true },
    visiting_company: { type: String, required: true },
    company_belongs_to: { type: String, required: true }
});

export const ClientVisitors = mongoose.model<IClientVisitors>('ClientVisitors', ClientVisitorsSchema);