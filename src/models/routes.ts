import * as mongoose from 'mongoose';

export interface IRoleBasedRoutes extends mongoose.Document {
    role: string;
    routes: string[];
};

const RoleBasedRoutesSchema = new mongoose.Schema({
    role: { type: String, required: true },
    routes: { type: Array, required: true }
});

export const RoleBasedRoutes = mongoose.model<IRoleBasedRoutes>('RoleBasedRoutes', RoleBasedRoutesSchema);