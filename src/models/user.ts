import * as mongoose from 'mongoose';

export type RoleEnum = 'ADMIN' | 'CLIENT';
type BelongsToEnum = 'SYNERGE_I' | 'SYNERGE_II';

export interface IUser extends mongoose.Document {
    first_name: string;
    last_name: string;
    user_name: string;
    password: string;
    email: string;
    phone: number;
    address?: string;
    city?: string;
    country?: string;
    postal_code?: string;
    role: RoleEnum;
    belongs_to?: BelongsToEnum;
    company_name?: string;
    activated: boolean;
    deleted: boolean;
    created_on: Date;
    updated_on: Date;
    profile_pic_path?: string;
    designation?: string;
    bio?: string;
};

const UserSchema = new mongoose.Schema({
    first_name: { type: String },
    last_name: { type: String },
    user_name: { type: String, required: true, unique: true },
    password: { type: String, required: true },
    email: { type: String },
    phone: { type: Number },
    address: { type: String, default: '' },
    city: { type: String, default: '' },
    country: { type: String, default: '' },
    postal_code: { type: String, default: '' },
    role: { type: String, default: '' },
    belongs_to: { type: String },
    company_name: { type: String },
    activated: { type: Boolean, required: true, default: false },
    deleted: { type: Boolean, required: true, default: false },
    created_on: { type: Date, required: true, default: new Date() },
    updated_on: { type: Date },
    profile_pic_path: { type: String },
    designation: { type: String },
    bio: { type: String }
});

export const User = mongoose.model<IUser>('User', UserSchema);