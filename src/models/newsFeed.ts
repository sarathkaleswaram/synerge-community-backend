import * as mongoose from 'mongoose';

interface Likes {
    by_user_id: string;
    by_user_name: string;
    date: Date;
};

interface Comments {
    comment: string;
    by_user_id: string;
    by_user_name: string;
    date: Date;
    reply?: Comments;
};

export interface INewsFeed extends mongoose.Document {
    by_user_id: string;
    by_user_name: string;
    date: Date;
    content: string;
    image_path?: string;
    likes: Likes[];
    comments: Comments[];
};

const NewsFeedSchema = new mongoose.Schema({
    by_user_id: { type: String, required: true },
    by_user_name: { type: String, required: true },
    date: { type: Date, required: true, default: new Date() },
    content: { type: String, required: true },
    image_path: { type: String },
    likes: { type: Array, required: true, default: [] },
    comments: { type: Array, required: true, default: [] }
});

export const NewsFeed = mongoose.model<INewsFeed>('NewsFeed', NewsFeedSchema);