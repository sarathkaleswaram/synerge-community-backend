import * as mongoose from 'mongoose';

type BelongsToEnum = 'SYNERGE_I' | 'SYNERGE_II';
type ActiveStatusEnum = 'Active' | 'Inactive';

export interface IClients extends mongoose.Document {
    company_name: string;
    belongs_to: BelongsToEnum;
    active_status: ActiveStatusEnum;
    joined_date: Date;
    leaving_date?: Date;
};

const ClientsSchema = new mongoose.Schema({
    company_name: { type: String, required: true },
    belongs_to: { type: String, required: true },
    active_status: { type: String, required: true },
    joined_date: { type: Date, required: true },
    leaving_date: { type: Date },
});

export const Clients = mongoose.model<IClients>('Clients', ClientsSchema);