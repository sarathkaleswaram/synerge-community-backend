import * as mongoose from 'mongoose';

interface Likes {
    by_user_id: string;
    by_user_name: string;
    date: Date;
};

interface Comments {
    comment: string;
    by_user_id: string;
    by_user_name: string;
    date: Date;
    reply?: Comments;
};

type EventTypeEnum = 'Upcoming' | 'Completed';
type BelongsToEnum = 'SYNERGE_I' | 'SYNERGE_II';

export interface IEvent extends mongoose.Document {
    date: Date;
    belongs_to: BelongsToEnum;
    type: EventTypeEnum;
    title: string;
    description: string;
    pics: string[];
    links: string[];
    likes: Likes[];
    comments: Comments[];
};

const EventSchema = new mongoose.Schema({
    date: { type: Date, required: true },
    belongs_to: { type: String, required: true },
    type: { type: String },
    title: { type: String, required: true },
    description: { type: String },
    pics: { type: Array },
    links: { type: Array },
    likes: { type: Array, required: true, default: [] },
    comments: { type: Array, required: true, default: [] }
});

export const Event = mongoose.model<IEvent>('Event', EventSchema);