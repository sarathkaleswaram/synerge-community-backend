import * as mongoose from 'mongoose';

type RoleEnum = 'ADMIN' | 'CLIENT';
type BelongsToEnum = 'SYNERGE_I' | 'SYNERGE_II';

export interface INotificationMobile extends mongoose.Document {
  token: string,
  createDate: Date,
  user_id: string,
  role: RoleEnum;
  belongs_to?: BelongsToEnum;
  company_name?: string;
};

const NotificationMobileSchema = new mongoose.Schema({
  token: { type: String, required: true },
  createDate: { type: Date, default: Date.now },
  user_id: { type: String, required: true },
  role: { type: String, required: true },
  belongs_to: { type: String },
  company_name: { type: String }
});

export const NotificationMobile = mongoose.model<INotificationMobile>('NotificationMobile', NotificationMobileSchema);