import * as mongoose from 'mongoose';

type BelongsToEnum = 'SYNERGE_I' | 'SYNERGE_II';

export interface IAchiever extends mongoose.Document {
    achiever: string;
    achieved: string;
    belongs_to: BelongsToEnum;
    company_name: string;
    date: Date;
    pic: string;
};

const AchieverSchema = new mongoose.Schema({
    achiever: { type: String, required: true },
    achieved: { type: String, required: true },
    belongs_to: { type: String, required: true },
    company_name: { type: String, required: true },
    date: { type: Date, required: true },
    pic: { type: String }
});

export const Achiever = mongoose.model<IAchiever>('Achiever', AchieverSchema);