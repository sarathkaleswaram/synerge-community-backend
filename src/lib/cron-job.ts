import * as moment from 'moment';
import { config } from '../app';
import { Event } from '../models/events';
import { Bookings } from '../models/bookings';
import { logger } from './logger';
import { pushFcmNotification } from './firebase-notification';
import { FCMEventFormat } from 'domains/response-format';

export const updateCompletedStatus = async () => {
  try {
    let events = await Event.updateMany({ type: 'Upcoming', date: { $lte: moment() } }, { type: 'Completed' }).exec();
    logger.debug('Events updated object: ', events);
    let bookings = await Bookings.updateMany({ status: 'Confirmed', booking_date: { $lte: moment() } }, { status: 'Completed' }).exec();
    logger.debug('Bookings updated object: ', bookings);
  } catch (error) {
    logger.error(error);
  }
};

export const sendGoodMorning = async () => {
  try {
    if (config.nodeEnv !== 'production') return;
    let fcm: FCMEventFormat = {
      title: 'GOOD MORNING',
      body: 'Have A Great Day',
      data: {
        path: '/tabs/news-feed'
      },
      image: undefined
    };
    pushFcmNotification('ALL', undefined, fcm);
    // pushFcmNotification('SYNERGE_I', undefined, fcm);
    // pushFcmNotification('SYNERGE_II', undefined, fcm);
  } catch (error) {
    logger.error(error);
  }
};