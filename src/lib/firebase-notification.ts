import * as admin from "firebase-admin";
import { config } from '../app';
import { logger } from './logger';
import { FCMEventFormat } from '../domains/response-format';
import { AppConstants } from '../domains/app-constants';
import { NotificationMobile, INotificationMobile } from '../models/notificationMobile';

const serviceAccount = require("../../firebase.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount)
});

export const registerUserToken = (req, res) => {
  NotificationMobile.findOne({ user_id: res.locals.jwtPayload._id, token: req.body.token }, (err, notificationToken) => {
    if (err) {
      logger.error('FCM Notification Subscription error:', err);
      return res.json({ success: false, message: 'FCM Notification Subscription error', data: err });
    }
    if (notificationToken) {
      res.json({ success: true, message: 'Previous FCM Notification Subscription available' });
    } else {
      let notificationMobile: INotificationMobile = new NotificationMobile({
        token: req.body.token,
        createDate: new Date(),
        user_id: res.locals.jwtPayload._id,
        role: res.locals.jwtPayload.role,
      });

      if (res.locals.jwtPayload.belongs_to)
        notificationMobile.belongs_to = res.locals.jwtPayload.belongs_to;
      if (res.locals.jwtPayload.company_name)
        notificationMobile.company_name = res.locals.jwtPayload.company_name;

      notificationMobile.save((err, notificationMobile) => {
        if (err) logger.error(err);
        logger.debug(`Notification token added for username: '${res.locals.jwtPayload.user_name}'`);
        res.json({ success: true, message: 'Notification Subscription successfully' });
      });
    }
  });
}

export const pushFcmNotification = (sendTo, id, fcm: FCMEventFormat) => {
  try {
    if (config.nodeEnv !== 'production') return;
    logger.debug("FCM sendTo: " + sendTo + ", id: " + id);
    if (id) {
      NotificationMobile.find({ user_id: id }, (err, notifications) => {
        if (!err) {
          let message = {
            notification: {
              title: fcm.title,
              body: fcm.body,
            },
            data: { ...fcm.data, id: id.toString() },
            android: {
              notification: {
                image: fcm.image ? `https://api.synergeworkspace.com${fcm.image}` : undefined,
                click_action: 'FCM_PLUGIN_ACTIVITY',
                icon: 'notification_icon',
                color: '#b80203'
              }
            },
            topic: id.toString(),
          };
          logger.debug('Sending FCM push notifications to topic id: ' + id);
          admin.messaging().send(message)
            .then((response) => {
              logger.debug('Successfully sent FCM Id message: ' + response);
            })
            .catch((error) => {
              logger.error('Error sending FCM Id message:', error);
            });
          // let tokens = notifications.map(n => (n.token));
          // if (tokens.length) {
          //   logger.debug('Sending FCM push notifications to single user', tokens);
          //   let multicastMessage = {
          //     notification: {
          //       title: fcm.title,
          //       body: fcm.body,
          //     },
          //     tokens: tokens,
          //     data: { ...fcm.data, id },
          //     android: {
          //       notification: {
          //         image: fcm.image ? `https://api.synergeworkspace.com${fcm.image}` : undefined,
          //         click_action: 'FCM_PLUGIN_ACTIVITY',
          //         icon: 'notification_icon',
          //         color: '#b80203'
          //       }
          //     },
          //   };
          //   admin.messaging().sendMulticast(multicastMessage)
          //     .then((response) => {
          //       logger.debug('Successfully sent FCM Multicast message: ', response);
          //       logger.debug("FCM Multicast: " + response.successCount + ' messages were sent successfully');
          //     })
          //     .catch((error) => {
          //       logger.error('Error sending FCM Multicast message: ', error);
          //     });
          // }
        }
      });
    }
    if (sendTo) {
      let topicName = '';
      switch (sendTo) {
        case AppConstants.SOCKET_ROOM_ADMIN:
          topicName = AppConstants.ROLE_ADMIN;
          break;
        case AppConstants.SOCKET_ROOM_ADMIN + 'SYNERGE_I':
          topicName = AppConstants.ROLE_ADMIN;
          topicName = 'SYNERGE_I';
          break;
        case AppConstants.SOCKET_ROOM_ADMIN + 'SYNERGE_II':
          topicName = AppConstants.ROLE_ADMIN;
          topicName = 'SYNERGE_II';
          break;
        case sendTo === 'SYNERGE_I' || sendTo === 'SYNERGE_II':
          topicName = sendTo;
          break;
        default:
          topicName = sendTo;
          break;
      }

      let message = {
        notification: {
          title: fcm.title,
          body: fcm.body,
        },
        data: fcm.data,
        android: {
          notification: {
            image: fcm.image ? `https://api.synergeworkspace.com${fcm.image}` : undefined,
            click_action: 'FCM_PLUGIN_ACTIVITY',
            icon: 'notification_icon',
            color: '#b80203'
          }
        },
        topic: topicName,
        //   token: registrationToken,
      };
      logger.debug('Sending FCM push notifications to topic: ' + topicName);
      admin.messaging().send(message)
        .then((response) => {
          logger.debug('Successfully sent FCM SendTo message: ' + response);
        })
        .catch((error) => {
          logger.error('Error sending FCM SendTo message:', error);
        });
    }
  } catch (error) {
    logger.error("FCM error: ", error);
  }
}