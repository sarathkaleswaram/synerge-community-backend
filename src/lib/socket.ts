import { io } from '../app';
import { logger } from './logger';
import { AppConstants } from '../domains/app-constants';
import { verifyJwt } from '../middleware';
import { SocketEventFormat } from '../domains/response-format';
import { pushNotification } from './webPush';

class SynergeSocket {

    public userSockets = [];

    public handleSocket() {
        io.on('connection', (socket) => {
            logger.debug('New Client Connected with Socket Id: ' + socket.id);
            socket.on(AppConstants.SOCKET_USER, (data) => {
                // logger.debug("Connected Socket User details", data);
                let tokenData = verifyJwt(data.token);
                if (tokenData) {
                    logger.debug('User connected with Socket Id: ' + socket.id + ", User Id: " + tokenData._id + ", User full name: " + data.full_name + ", Role: " + tokenData.role);
                    let userSocket: any = socket;
                    userSocket.user_id = tokenData._id;
                    userSocket.role = tokenData.role;
                    userSocket.belongs_to = tokenData.belongs_to;
                    userSocket.company_name = tokenData.company_name;
                    this.userSockets.push(userSocket);
                    if (tokenData.role === AppConstants.ROLE_ADMIN) {
                        socket.join(AppConstants.SOCKET_ROOM_ADMIN);
                        socket.join(AppConstants.SOCKET_ROOM_ADMIN + tokenData.belongs_to);
                    } else {
                        socket.join(tokenData.belongs_to);
                        socket.join(tokenData.company_name);
                    }
                    logger.verbose("Total socket users: " + this.userSockets.length);
                }
            });
            socket.on('disconnect', () => {
                logger.debug('Client disconnected with Socket Id: ' + socket.id);
                this.userSockets.splice(this.userSockets.findIndex(x => x.id === socket.id), 1);
                logger.verbose("Total socket users: " + this.userSockets.length);
            });
        });
    }

    public sendSocketResponse(socketResponse: SocketEventFormat, sendToRoom: string, id: string) {
        logger.debug("Socket Response sendToRoom: " + sendToRoom + ", id: " + id);
        // push notification
        pushNotification(sendToRoom, id, socketResponse);
        if (sendToRoom) {
            // Send to Rooms - ADMIN, BelongsTo, Company name...
            logger.debug("Send Socket Response To Room: " + sendToRoom);
            io.to(sendToRoom).emit(AppConstants.SOCKET_EVENT, socketResponse);
        } else if (id) {
            // Send to individual
            let userSocket = this.userSockets.find(x => x.user_id === id);
            if (userSocket) {
                if (userSocket.role !== 'ADMIN') {
                    logger.debug("Send Socket Response to Individual id: " + id + ", Socket Id: " + userSocket.id);
                    userSocket.emit(AppConstants.SOCKET_EVENT, socketResponse);
                }
            }
        }
        // if (role === 'ADMIN') {
        // let sockets = <socket.Socket[]>this.userSockets.filter(x => x.role === AppConstants.ROLE_ADMIN);
        // console.log(sockets, '-----------------------------sockets')
        // sockets.forEach(socket => {
        //     console.log(socket, '-----------------------------socket')
        //     // Send to all clients except sender
        //     socket.broadcast.emit(AppConstants.SOCKET_EVENT, socketResponse);
        //     // Send to all clients include sender
        //     socket.emit(AppConstants.SOCKET_EVENT, socketResponse);
        // });
        // }
    }
}

export default new SynergeSocket();
