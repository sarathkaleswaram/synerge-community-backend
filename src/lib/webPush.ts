import * as webpush from 'web-push';
import { logger } from './logger';
import { NotificationKeys, INotificationKeys } from '../models/notificationKeys';
import { SocketEventFormat } from '../domains/response-format';
import { AppConstants } from '../domains/app-constants';

const notificationPayload = {
  notification: {
    title: 'Update in SYNERGE',
    body: '',
    icon: 'assets/img/favicon.png',
  },
}

export const registerUserSW = (req, res) => {
  NotificationKeys.findOne({ user_id: res.locals.jwtPayload._id, endpoint: req.body.endpoint }, (err, notificationKey) => {
    if (err) {
      logger.error('Notification Subscription error:', err);
      return res.json({ success: false, message: 'Notification Subscription error', data: err });
    }
    if (notificationKey) {
      res.json({ success: true, message: 'Previous Notification Subscription available' });
    } else {
      let notificationKeys: INotificationKeys = new NotificationKeys({
        endpoint: req.body.endpoint,
        keys: req.body.keys,
        createDate: new Date(),
        user_id: res.locals.jwtPayload._id,
        role: res.locals.jwtPayload.role,
      });

      if (res.locals.jwtPayload.belongs_to)
        notificationKeys.belongs_to = res.locals.jwtPayload.belongs_to;
      if (res.locals.jwtPayload.company_name)
        notificationKeys.company_name = res.locals.jwtPayload.company_name;

      notificationKeys.save((err, notificationKeys) => {
        if (err) logger.error(err);
        logger.debug(`Notification Key added for username: '${res.locals.jwtPayload.user_name}'`);
        res.json({ success: true, message: 'Notification Subscription successfully' });
      });
    }

  });
}

export const pushNotification = (sendTo, id, response: SocketEventFormat) => {
  if (id) {
    NotificationKeys.find({ user_id: id }, (err, notifications) => {
      if (!err) {
        logger.debug('Sending Web push notifications to single user');
        notificationPayload.notification.body = response.message;
        Promise.all(notifications.map(notification =>
          webpush.sendNotification(
            { endpoint: notification.endpoint, keys: notification.keys },
            JSON.stringify(notificationPayload)
          )))
          .then(() => { })
          .catch(err => { });
      }
    });
  }
  if (sendTo) {
    let query = {};
    switch (sendTo) {
      case AppConstants.SOCKET_ROOM_ADMIN:
        query['role'] = AppConstants.ROLE_ADMIN;
        break;
      case AppConstants.SOCKET_ROOM_ADMIN + 'SYNERGE_I':
        query['role'] = AppConstants.ROLE_ADMIN;
        query['belongs_to'] = 'SYNERGE_I';
        break;
      case AppConstants.SOCKET_ROOM_ADMIN + 'SYNERGE_II':
        query['role'] = AppConstants.ROLE_ADMIN;
        query['belongs_to'] = 'SYNERGE_II';
        break;
      case sendTo === 'SYNERGE_I' || sendTo === 'SYNERGE_II':
        query['belongs_to'] = sendTo;
        break;
      default:
        query['company_name'] = sendTo;
        break;
    }
    NotificationKeys.find(query, (err, notifications) => {
      if (!err) {
        logger.debug('Sending Web push notifications to users count: ' + notifications.length);
        notificationPayload.notification.body = response.message;
        Promise.all(notifications.map(notification =>
          webpush.sendNotification(
            { endpoint: notification.endpoint, keys: notification.keys },
            JSON.stringify(notificationPayload)
          )))
          .then(() => { })
          .catch(err => { });
      }
    });
  }
}