import * as nodemailer from 'nodemailer';
import { Request, Response } from 'express';
import * as jwt from 'jsonwebtoken';
import { config } from '../app';
import { logger } from './logger';
import forgetPassword from '../emails/forget-password';
import { User } from '../models/user';
import { ResponseFormat } from 'domains/response-format';


const sendResponse = (result: ResponseFormat, res: Response) => {
  res.json(result);
}

export const sendForgotPassword = async (req: Request, res: Response) => {
  try {
    logger.debug("Forgot password");
    if (!req.body.email) return sendResponse({ success: false, message: 'Email missing' }, res);
    let user = await User.findOne({ email: req.body.email }).exec();
    if (!user) return sendResponse({ success: false, message: 'Invalid Email address' }, res);

    let transporter = nodemailer.createTransport({
      service: 'gmail',
      auth: {
        user: 'synergeworkspace@gmail.com',
        pass: 'synergeworkspace18122018'
      }
    });
    // transporter.verify(function (error, success) {
    //   if (error) {
    //     console.log(error);
    //   } else {
    //     console.log('Server is ready to take our messages');
    //   }
    // });

    const token = jwt.sign(
      {
          _id: user._id,
          user_name: user.user_name,
          email: user.email,
          // role: user.role,
          // belongs_to: user.belongs_to,
          // company_name: user.company_name
      },
      config.jwtEmailSecret,
      { expiresIn: config.jwtExpiresIn }
  );

    let name = `${user.first_name} ${user.last_name}`;
    let url = `http://community.synergeworkspace.com/reset-password?token=${token}`;

    let mailOptions = {
      from: 'synergeworkspace@gmail.com',
      to: user.email,
      subject: `Synerge Community Reset Password.`,
      html: forgetPassword(name, url)
    };

    transporter.sendMail(mailOptions, function (error, info) {
      if (error) {
        logger.error("email error: ", error);
        return sendResponse({ success: false, message: error.message || JSON.stringify(error) }, res);
      } else {
        logger.debug('Email sent to: ' + user.email + ', response: ' + info.response);
        return sendResponse({ success: true, message: 'Sent you an Email to Reset your password.' }, res);
      }
    });
  } catch (error) {
    logger.error('Email sent failed: ' + error);
    return sendResponse({ success: false, message: error.message || JSON.stringify(error) }, res);
  }
}