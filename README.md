# Synerge Community Backend

## Start the server

Run in development mode

```
npm run start:watch
```

Run in production mode 

```
npm run prod
```

## Notification setup
Medium - https://medium.com/enappd/firebase-push-notifications-in-ionic-apps-d90c6550fdb7
Docs - https://firebase.google.com/docs/cloud-messaging/send-message#node.js